<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->integer('total_views')->default(0);
            $table->string('event');
            $table->string('host');
            $table->datetime('starting');
            $table->datetime('ending');
            $table->string('image');
            $table->string('venue');
            $table->text('description');
            $table->string('audience');
            $table->string('categories');
            $table->string('slug');
            $table->string('cost')->default('free');
            $table->string('url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
