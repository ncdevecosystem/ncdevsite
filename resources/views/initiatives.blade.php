<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/tailwindcss@1.9.6/dist/tailwind.min.css">
    <link href="https://unpkg.com/pattern.css" rel="stylesheet">  
    <link href="./css/app.css" rel="stylesheet">
    <!-- Styles -->

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <!-- component -->
<link rel="stylesheet" href="https://demos.creative-tim.com/notus-js/assets/styles/tailwind.css">
<link rel="stylesheet" href="https://demos.creative-tim.com/notus-js/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
<style>
    body {
        overflow-x: hidden;
    }

    .font-size38 {
    font-size: 38px;
}
.team-single-text .section-heading h4,
.section-heading h5 {
    font-size: 36px
}

.team-single-text .section-heading.half {
    margin-bottom: 20px
}

@media screen and (max-width: 1199px) {
    .team-single-text .section-heading h4,
    .section-heading h5 {
        font-size: 32px
    }
    .team-single-text .section-heading.half {
        margin-bottom: 15px
    }
}

@media screen and (max-width: 991px) {
    .team-single-text .section-heading h4,
    .section-heading h5 {
        font-size: 28px
    }
    .team-single-text .section-heading.half {
        margin-bottom: 10px
    }
}

@media screen and (max-width: 767px) {
    .team-single-text .section-heading h4,
    .section-heading h5 {
        font-size: 24px
    }
}


.team-single-icons ul li {
    display: inline-block;
    border: 1px solid #02c2c7;
    border-radius: 50%;
    color: #86bc42;
    margin-right: 8px;
    margin-bottom: 5px;
    -webkit-transition-duration: .3s;
    transition-duration: .3s
}

.team-single-icons ul li a {
    color: #02c2c7;
    display: block;
    font-size: 14px;
    height: 25px;
    line-height: 26px;
    text-align: center;
    width: 25px
}

.team-single-icons ul li:hover {
    background: #02c2c7;
    border-color: #02c2c7
}

.team-single-icons ul li:hover a {
    color: #fff
}

.team-social-icon li {
    display: inline-block;
    margin-right: 5px
}

.team-social-icon li:last-child {
    margin-right: 0
}

.team-social-icon i {
    width: 30px;
    height: 30px;
    line-height: 30px;
    text-align: center;
    font-size: 15px;
    border-radius: 50px
}

.padding-30px-all {
    padding: 30px;
}
.bg-light-gray {
    background-color: #f7f7f7;
}
.text-center {
    text-align: center!important;
}
img {
    max-width: 100%;
    height: auto;
}


.list-style9 {
    list-style: none;
    padding: 0
}

.list-style9 li {
    position: relative;
    padding: 0 0 15px 0;
    margin: 0 0 15px 0;
    border-bottom: 1px dashed rgba(0, 0, 0, 0.1)
}

.list-style9 li:last-child {
    border-bottom: none;
    padding-bottom: 0;
    margin-bottom: 0
}


.text-sky {
    color: #02c2c7
}

.text-orange {
    color: #e95601
}

.text-green {
    color: #5bbd2a
}

.text-yellow {
    color: #f0d001
}

.text-pink {
    color: #ff48a4
}

.text-purple {
    color: #9d60ff
}

.text-lightred {
    color: #ff5722
}

a.text-sky:hover {
    opacity: 0.8;
    color: #02c2c7
}

a.text-orange:hover {
    opacity: 0.8;
    color: #e95601
}

a.text-green:hover {
    opacity: 0.8;
    color: #5bbd2a
}

a.text-yellow:hover {
    opacity: 0.8;
    color: #f0d001
}

a.text-pink:hover {
    opacity: 0.8;
    color: #ff48a4
}

a.text-purple:hover {
    opacity: 0.8;
    color: #9d60ff
}

a.text-lightred:hover {
    opacity: 0.8;
    color: #ff5722
}

.custom-progress {
    height: 10px;
    border-radius: 50px;
    box-shadow: none;
    margin-bottom: 25px;
}
.progress {
    display: -ms-flexbox;
    display: flex;
    height: 1rem;
    overflow: hidden;
    font-size: .75rem;
    background-color: #e9ecef;
    border-radius: .25rem;
}


.bg-sky {
    background-color: #02c2c7
}

.bg-orange {
    background-color: #e95601
}

.bg-green {
    background-color: #5bbd2a
}

.bg-yellow {
    background-color: #f0d001
}

.bg-pink {
    background-color: #ff48a4
}

.bg-purple {
    background-color: #9d60ff
}

.bg-lightred {
    background-color: #ff5722
}
</style>
    
<section class="relative  bg-gray-100 ">
    <div class="relative pt-16 pb-32 flex content-center items-center justify-center min-h-screen-75">
            <div class="absolute top-0 w-full h-full bg-center bg-cover" style="
                background-image: url('https://live.staticflickr.com/65535/48054153492_d4058de5fc_c_d.jpg');
              ">
    
              <span id="blackOverlay" class="w-full h-full absolute opacity-75 bg-black"></span>
            </div>
            <div class="container relative mx-auto">
              <div class="items-center flex flex-wrap">
                <div class="w-full lg:w-6/12 px-4 ml-auto mr-auto text-center">
                  <div class="pr-12">
                    <h1 class="text-white font-semibold text-5xl font-sans ">
                Summer Dev Days 2021
                    </h1>
                    <p class="mt-4 text-lg text-blueGray-200 font-sans ">
                        SummerDevDays will provide a platform for aspiring individuals and programmers to learn and build using open source tools and Geekulcha partners' tools
                    </p><br><br>
                    <a href="#about" class="bg-red-500 px-2 py-2 rounded text-white hover:bg-gray-900">Read More</a>
                    <a href="#register" class="bg-transparent px-2 py-2 rounded  border-white border-2 text-white hover:bg-red-500">Register Now</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="top-auto bottom-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden h-70-px" style="transform: translateZ(0px)">
              <svg class="absolute bottom-0 overflow-hidden" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" version="1.1" viewBox="0 0 2560 100" x="0" y="0">
                <polygon class="text-red-500 fill-current" points="2560 0 2560 100 0 100"></polygon>
              </svg>
            </div>
          </div>

          <section class="pb-10 bg-red-500 -mt-24">
            <div class="container mx-auto px-4">
              <div class="flex flex-wrap">
                <div class="lg:pt-12 pt-6 w-full md:w-4/12 px-4 text-center">
                  <div class="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                    <div class="px-4 py-5 flex-auto">
                      <div class="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-red-400">
                        <i class="fas fa-award"></i>
                      </div>
                      <h6 class="text-xl font-semibold">Open Source Tools</h6>
                      <p class="mt-2 mb-4 text-blueGray-500">
                        Participants are enouraged to make use of open source tools and 
                    tools provided by our partners.
                        
                      </p>
                    </div>
                  </div>
                </div>
                <div class="w-full md:w-4/12 px-4 text-center">
                  <div class="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                    <div class="px-4 py-5 flex-auto">
                      <div class="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-lightBlue-400">
                        <i class="fas fa-retweet"></i>
                      </div>
                      <h6 class="text-xl font-semibold">Code From Home</h6>
                      <p class="mt-2 mb-4 text-blueGray-500">
                        #SummerDevDays will be hosted both virtually and physically on the 01 - 05 December 2021
                      </p>
                    </div>
                  </div>
                </div>
                <div class="pt-6 w-full md:w-4/12 px-4 text-center">
                  <div class="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                    <div class="px-4 py-5 flex-auto">
                      <div class="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-emerald-400">
                        <i class="fas fa-fingerprint"></i>
                      </div>
                      <h6 class="text-xl font-semibold">African Devs</h6>
                      <p class="mt-2 mb-4 text-blueGray-500">
                       Open to developers from all parts of the African continent to connect through Code
                      </p>
                   
                    </div>
                  </div>
                </div>
              </div>
            
             
          </section>
        <!-- component -->
    <style>
      .pt-\[17\%\] {
        padding-top: 17%;
      }
      .mt-\[-10\%\] {
        margin-top: -10%;
      }
      .pt-\[56\.25\%\] {
        padding-top: 56.25%;
      }
    </style>
    <div class="container" id="about"><br><br>
      <div class="row">
        <div class="col-lg-12 m-auto" style="text-align: left!important;">
           <h1 class="text-3xl ">
              About Summer Dev Days 2021
           </h1><br>
           <div class="h-1 w-20 bg-red-500 rounded col-lg-12 m-auto text-center "></div>
        </div>

     </div><br><br><br></div>

     {{-- ABOUT SUMMER DEV DAYS  --}}

     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" integrity="sha256-h20CPZ0QyXlBuAw7A+KluUYx/3pK+c7lYEpqLTlxjYQ=" crossorigin="anonymous" />
     <div class="container">
         <div class="team-single">
             <div class="row">
                 <div class="col-lg-4 col-md-5 xs-margin-30px-bottom">
                     <div class="team-single-img">
                         <img src="https://live.staticflickr.com/4902/31217700487_a92d1e92f6_w_d.jpg" alt="">
                     </div>
                     <div class="bg-light-gray padding-30px-all md-padding-25px-all sm-padding-20px-all text-left">
                         <h4 class="margin-10px-bottom font-size24 md-font-size22 sm-font-size20 font-weight-600"><b>Please note...</b></h4>
                         <br><p class="sm-width-95 sm-margin-auto">
                          This is not a Hackathon and there is no winner, it's a community building event to build together and build each other. </p>
                     </div>
                 </div>
     
                 <div class="col-lg-8 col-md-7">
                     <div class="team-single-text padding-50px-left sm-no-padding-left">
                         <h4 class="font-size38 sm-font-size32 xs-font-size30" style="color: #6a6a6a;"><b>Finish What You Sta...</b></h4>
                         <br><br>
                         <p class="no-margin-bottom">Almost anything you can think of, technology can help make it possible. It's a think it, code it world which is run on software. We need to equip ourselves for the digital world and equally, South African developers need to master their own craft and build solutions that respond to our country's needs. <br><br>

                            To this effect, Geekulcha has created the Summer Dev Days platform to signal the change in season and caution local developers towards actuation and completion of projects or learning journeys started earlier in the year.
                            <br><br>
                            The Summer Dev Days coding festival events have previously been hosted in Kimberley and Pretoria. At the last physical edition in 2018, Minister Stella Ndabeni-Abrahams reminded local techies that "There cannot be a 4IR without coders". Geekulcha has since taken a stance of "Living 4IR" to encourage its community to be contextual and seek to develop localised solutions that enterprises mankind.</p>
                            <br><br><br>
                         <div class="contact-info-section margin-40px-tb">
                             <ul class="list-style9 no-margin">
                                 <li>
     
                                     <div class="row">
                                         <div class="col-md-5 col-5">
                                             <i class="fas fa-question-circle text-orange"></i>
                                             <strong class="margin-10px-left text-orange">What</strong>
                                         </div>
                                         <div class="col-md-7 col-7">
                                             <p>A developer festival, challenging South African techies to finish what they started in 2021</p>
                                         </div>
                                     </div>
     
                                 </li>
                                 <li>
     
                                     <div class="row">
                                         <div class="col-md-5 col-5">
                                             <i class="far fa-gem text-yellow"></i>
                                             <strong class="margin-10px-left text-yellow">Who</strong>
                                         </div>
                                         <div class="col-md-7 col-7">
                                             <p>Upcoming and advanced developers</p><br>
                                         </div>
                                     </div>
     
                                 </li>
                                 <li>
     
                                     <div class="row">
                                         <div class="col-md-5 col-5">
                                             <i class="far fa-file text-lightred"></i>
                                             <strong class="margin-10px-left text-lightred">Where</strong>
                                         </div>
                                         <div class="col-md-7 col-7">
                                             <p>Virtual and Physical</p><br>
                                         </div>
                                     </div>
     
                                 </li>
                                 <li>
     
                                     <div class="row">
                                         <div class="col-md-5 col-5">
                                             <i class="fas fa-map-marker-alt text-green"></i>
                                             <strong class="margin-10px-left text-green">When</strong>
                                         </div>
                                         <div class="col-md-7 col-7">
                                             <p>01 December 2021 (Wed, 18:00) - 05 December 2021 (Sun, 17:00PM)</p>
                                         </div>
                                     </div>
     
                                 </li>
                                 <li>
     
                                     <div class="row">
                                         <div class="col-md-5 col-5">
                                             <i class="fas fa-mobile-alt text-purple"></i>
                                             <strong class="margin-10px-left xs-margin-four-left text-purple">Why</strong>
                                         </div>
                                         <div class="col-md-7 col-7">
                                             <p>Helping developers finish learning and projects to be at the hands of users</p>
                                         </div>
                                     </div>
     
                                 </li>
                                 <li>
                                     <div class="row">
                                         <div class="col-md-5 col-5">
                                             <i class="fas fa-envelope text-pink"></i>
                                             <strong class="margin-10px-left xs-margin-four-left text-pink">How</strong>
                                         </div>
                                         <div class="col-md-7 col-7">
                                             <p><a href="javascript:void(0)">Register indicating your project or learning; Attend DevLabs for further knowledge; Code/Build; Present on the last day.</a></p>
                                         </div>
                                     </div>
                                 </li>
                             </ul>
                         </div>
     <br><br><br><br><!--
     <h3><b>Focus Outset</b></h3><br><br>
                         <h5 class="font-size24 sm-font-size22 xs-font-size20">Professional Skills</h5><br>
     
                         <div class="sm-no-margin">
                             <div class="progress-text">
                                 <div class="row">
                                     <div class="col-7">Positive Behaviors</div>
                                     <div class="col-5 text-right">40%</div>
                                 </div>
                             </div>
                             <div class="custom-progress progress">
                                 <div role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:40%" class="animated custom-bar progress-bar2 slideInLeft bg-sky"></div>
                             </div>
                             <div class="progress-text">
                                 <div class="row">
                                     <div class="col-7">Teamworking Abilities</div>
                                     <div class="col-5 text-right">50%</div>
                                 </div>
                             </div>
                             <div class="custom-progress progress">
                                 <div role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:50%" class="animated custom-bar progress-bar2 slideInLeft bg-orange"></div>
                             </div>
                             <div class="progress-text">
                                 <div class="row">
                                     <div class="col-7">Time Management </div>
                                     <div class="col-5 text-right">60%</div>
                                 </div>
                             </div>
                             <div class="custom-progress progress">
                                 <div role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:60%" class="animated custom-bar progress-bar2 slideInLeft bg-green"></div>
                             </div>
                             <div class="progress-text">
                                 <div class="row">
                                     <div class="col-7">Excellent Communication</div>
                                     <div class="col-5 text-right">80%</div>
                                 </div>
                             </div>
                             <div class="custom-progress progress">
                                 <div role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:80%" class="animated custom-bar progress-bar2 slideInLeft bg-yellow"></div>
                             </div>
                         </div>
     
                     </div>
                 </div>-->
     
                 <div class="col-md-12">
     
                 </div>
             </div>
         </div>
     </div>
</section>
        <!-- component -->
    <link rel="stylesheet" href="https://demos.creative-tim.com/notus-js/assets/styles/tailwind.css">
    <link rel="stylesheet" href="https://demos.creative-tim.com/notus-js/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css">
    
    <div class="flex flex-wrap items-center pt-8">
      <div class="w-full md:w-6/12 px-4 mr-auto ml-auto ">
        <div class="justify-center flex flex-wrap relative">
          <div class="my-4 w-full lg:w-6/12 px-4">
            <a href="" target="_blank">
              <div class="
              shadow-lg rounded-lg text-center p-1">
                <div class="w-full md:w-2/5 h-40">
                  <img class="object-center object-cover w-full h-full" src="https://live.staticflickr.com/4906/45418877234_faaa3a12d2_4k.jpg&auto=format&fit=crop&w=1170&q=80" alt="photo">
              </div>
              </div>
            </a>
            <a  target="_blank">
              <div class="shadow-lg rounded-lg text-center p-1 mt-8">
                <div class="w-full md:w-2/5 h-40">
                  <img class="object-center object-cover w-full h-full" src="https://live.staticflickr.com/4819/45244846715_c3202b7115_z.jpg&auto=format&fit=crop&w=1170&q=80" alt="photo">
              </div>
              </div>
            </a>
            <a  target="_blank">
              <div class="shadow-lg rounded-lg text-center p-1 mt-8">
                <div class="w-full md:w-2/5 h-40">
                  <img class="object-center object-cover w-full h-full" src="https://live.staticflickr.com/4867/31217873727_a0b31dbdb8_z.jpg&auto=format&fit=crop&w=1170&q=80" alt="photo">
              </div>
              </div>
            </a>
          </div>
          <div class="my-4 w-full lg:w-6/12 px-4 lg:mt-16">
            <a href="" target="_blank">
              <div class="shadow-lg rounded-lg text-center p-1">
                <div class="w-full md:w-2/5 h-40">
                  <img class="object-center object-cover w-full h-full" src="https://live.staticflickr.com/4910/45433409574_a810cc86b3_z.jpg&auto=format&fit=crop&w=1170&q=80" alt="photo">
              </div>
              </div>
            </a>
            <a  target="_blank">
              <div class=" shadow-lg rounded-lg text-center p-1 mt-8">
                <div class="w-full md:w-2/5 h-40">
                  <img class="object-center object-cover w-full h-full" src="https://live.staticflickr.com/4836/45244914985_a40c6bc9d7_z.jpg&auto=format&fit=crop&w=1170&q=80" alt="photo">
              </div>
              </div>
            </a>
            <a  target="_blank">
              <div class="shadow-lg rounded-lg text-center p-1 mt-8">
                <div class="w-full md:w-2/5 h-40">
                  <img class="object-center object-cover w-full h-full" src="https://live.staticflickr.com/4830/46106604092_a7faa284a0_z.jpg&auto=format&fit=crop&w=1170&q=80" alt="photo">
              </div>
              </div>
            </a>
          </div>
        </div>
      </div>
    <br><br><br>
      <div class="w-full md:w-4/12 px-12 md:px-4 ml-auto mr-auto mt-16">
        <div class="text-blueGray-500 p-3 text-center inline-flex items-center justify-center w-16 h-16 mb-6 shadow-lg rounded-full bg-white">
          <i class="fas fa-drafting-compass text-xl"></i>
        </div>
        
        <h4 class="font-size38 sm-font-size32 xs-font-size30" style="color: #6a6a6a;"><b>Previous Summer Dev Days</b></h4>
        <p ><br><br>
          The last Summer Dev Days in 2018 had 136 participants which consisted of developers, high school learners, designers and government officials. 
          During the two days of coding participants learned from industry mentors not only about coding but also about the fourth Industrial Revolution (4IR), in which trends such as the Internet of things, robotics, virtual reality, and artificial intelligence are disrupting the way we live and work.
        <p ><br><br>
        <p>Some of the project highlights of the event include a TB test machine learning solution to help with efficient diagnosis of tuberculosis
          , a honeypot server to deceive cybercriminals and boost information security
         and a Data mining with an IOT underground lights system.</p>
      </div>
    </div>
    <footer class="relative pt-8 pb-6 mt-8">
      <div class="container mx-auto px-4">
        <div class="flex flex-wrap items-center md:justify-between justify-center">
          <div class="w-full md:w-6/12 px-4 mx-auto text-center">
           
          </div>
        </div>
      </div>
    </footer>
   
    <!-- component -->
    <section class="text-gray-900 body-font" style="background: #fafafa;">
      <div class="container px-5 py-24 mx-auto">
        <div class="flex flex-wrap w-full mb-8">
          <div class="w-full mb-6 lg:mb-0">
             <h1 class="text-3xl ">
              Dev Lab Sessions 
           </h1><br>
            <div class="h-1 w-20 bg-red-500 rounded col-lg-12 m-auto text-center "></div><br><br>
          </div>
        </div>
        <div class="flex flex-wrap -m-4">
          <div class="p-4 lg:w-1/3 w-1/2">
            <div class="flex rounded-lg h-full bg-gray-700 p-8 flex-col">
              <div class="flex items-center mb-3">
                <div class="w-8 h-8 mr-3 inline-flex items-center justify-center rounded-full bg-red-400 text-white flex-shrink-0">
                  <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
                    <path d="M22 12h-4l-3 9L9 3l-3 9H2"></path>
                  </svg>
                </div>
                <h2 class="text-white text-lg title-font font-medium">Microsoft Azure</h2>
              </div>
              <div class="flex-grow">
                <p class="leading-relaxed text-base text-white">Microsoft Azure, often referred to as Azure, is a cloud computing service operated by Microsoft for application management via Microsoft-managed data centers.</p>
                <a href="https://azure.microsoft.com/en-us/" class="mt-3 text-gray-100 hover:text-gray-100 inline-flex items-center">Learn More
                  <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-4 h-4 ml-2" viewBox="0 0 24 24">
                    <path d="M5 12h14M12 5l7 7-7 7"></path>
                  </svg>
                </a>
              </div>
            </div>
          </div>
          <div class="p-4 lg:w-1/3 w-1/2">
            <div class="flex rounded-lg h-full bg-gray-700 p-8 flex-col">
              <div class="flex items-center mb-3">
                <div class="w-8 h-8 mr-3 inline-flex items-center justify-center rounded-full bg-red-400 text-white flex-shrink-0">
                  <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
                    <path d="M20 21v-2a4 4 0 00-4-4H8a4 4 0 00-4 4v2"></path>
                    <circle cx="12" cy="7" r="4"></circle>
                  </svg>
                </div>
                <h2 class="text-white text-lg title-font font-medium">Lepsta Platform</h2>
              </div>
              <div class="flex-grow">
                <p class="leading-relaxed text-base text-white">Lepsta is a software development platform that helps developers produce high-quality software at a fast rate.</p>
                <a href="https://lepsta.tech/home/" class="mt-3 text-gray-100 hover:text-gray-100 inline-flex items-center">Learn More
                  <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-4 h-4 ml-2" viewBox="0 0 24 24">
                    <path d="M5 12h14M12 5l7 7-7 7"></path>
                  </svg>
                </a>
              </div>
            </div>
          </div><div class="p-4 lg:w-1/3 w-1/2">
            <div class="flex rounded-lg h-full bg-gray-700 p-8 flex-col">
              <div class="flex items-center mb-3">
                <div class="w-8 h-8 mr-3 inline-flex items-center justify-center rounded-full bg-red-400 text-white flex-shrink-0">
                  <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
                    <circle cx="6" cy="6" r="3"></circle>
                    <circle cx="6" cy="18" r="3"></circle>
                    <path d="M20 4L8.12 15.88M14.47 14.48L20 20M8.12 8.12L12 12"></path>
                  </svg>
                </div>
                <h2 class="text-white text-lg title-font font-medium">All things Apps</h2>
              </div>
              <div class="flex-grow">
                <p class="leading-relaxed text-base text-white">All things Apps including the Front-End, Back-End and technologies used for apps.</p>
  
                  <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-4 h-4 ml-2" viewBox="0 0 24 24">
                    <path d="M5 12h14M12 5l7 7-7 7"></path>
                  </svg>
                </a>
              </div>
            </div>
          </div>
          <div class="p-4 lg:w-1/3 w-1/2">
            <div class="flex rounded-lg h-full bg-gray-700 p-8 flex-col">
              <div class="flex items-center mb-3">
                <div class="w-8 h-8 mr-3 inline-flex items-center justify-center rounded-full bg-red-400 text-white flex-shrink-0">
                  <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
                    <circle cx="6" cy="6" r="3"></circle>
                    <circle cx="6" cy="18" r="3"></circle>
                    <path d="M20 4L8.12 15.88M14.47 14.48L20 20M8.12 8.12L12 12"></path>
                  </svg>
                </div>
                <h2 class="text-white text-lg title-font font-medium">Mapping Intelligence</h2>
              </div>
              <div class="flex-grow">
                <p class="leading-relaxed text-base text-white">Map Intelligence is a fast, highly configurable and efficient platform for delivering customised, map-enabled business applications involving BI tools and mapping providers and GIS. It is designed for dynamic use in a web-browser with or without BI integration.</p>
                
                  <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-4 h-4 ml-2" viewBox="0 0 24 24">
                    <path d="M5 12h14M12 5l7 7-7 7"></path>
                  </svg>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section><br><br><br>
<section id="register" style="background-image: url('images/events-header.svg')">
    <div class="">
        <div class="flex flex-col items-center flex-1 h-full justify-center px-4 sm:px-0">
            <div class="flex rounded-lg shadow-lg  sm:w-3/4 lg:w-1/2 bg-white sm:mx-0 col-md-8" >
                <div class="flex flex-col w-full md:w-1/2 p-4">
                    <div class="flex flex-col flex-1 justify-center mb-8">
                        <h1 class="text-4xl text-center font-thin">Register for #SummerDevDays</h1>
                       
                        <form action="https://reg.gklink.co/events/1">
                        <label>Email </label> <input  type="email" class="w-full px-4 py-2 mt-2 mr-4 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-gray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none focus:shadow-outline focus:ring-2 ring-offset-current ring-offset-2" name="email">
                        <label>First name </label>  <input class="w-full px-4 py-2 mt-2 mr-4 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-gray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none focus:shadow-outline focus:ring-2 ring-offset-current ring-offset-2" name="first_name">
                        <label>Last name </label>  <input class="w-full px-4 py-2 mt-2 mr-4 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-gray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none focus:shadow-outline focus:ring-2 ring-offset-current ring-offset-2" name="last_name">
                        <label>gender </label><br>
                        <input type="checkbox" name="gender" value="1">  Female<br>      
                        <input type="checkbox" name="gender" value="2">  Male<br>      
                        <input type="checkbox" name="gender" value="3">  Prefer not to specify<br><br>
                        <label>Organisation </label> <input class="w-full px-4 py-2 mt-2 mr-4 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-gray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none focus:shadow-outline focus:ring-2 ring-offset-current ring-offset-2" name="org">
                        <label>Role at organisation</label> <input class="w-full px-4 py-2 mt-2 mr-4 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-gray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none focus:shadow-outline focus:ring-2 ring-offset-current ring-offset-2" name="role">
                      
                        <div class="form-group">
                          <label for="african-countries">How did you hear about this event </i></label>
                          <select type="text" class="form-control " name="heard">
                          <option selected>Choose</option>
                          <option value="twitter">Twitter</option>
                          <option value="facebook">Facebook</option>
                          <option value="email">Email</option>
                          <option value="other">Other</option>
                          </select>
                          <label>age</label>  <input class="w-full px-4 py-2 mt-2 mr-4 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-gray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none focus:shadow-outline focus:ring-2 ring-offset-current ring-offset-2" name="age">
                        <label>git </label>  <input class="w-full px-4 py-2 mt-2 mr-4 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-gray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none focus:shadow-outline focus:ring-2 ring-offset-current ring-offset-2" name="git_account'">
                        <label>phone number </label><input  class="w-full px-4 py-2 mt-2 mr-4 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-gray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none focus:shadow-outline focus:ring-2 ring-offset-current ring-offset-2"name="phone_number">
                        <label>diet </label> <input class="w-full px-4 py-2 mt-2 mr-4 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-gray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none focus:shadow-outline focus:ring-2 ring-offset-current ring-offset-2" name="diet">
                        <label>background </label>  <input class="w-full px-4 py-2 mt-2 mr-4 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-gray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none focus:shadow-outline focus:ring-2 ring-offset-current ring-offset-2" name="background">
                        <label>team name </label>  <input  class="w-full px-4 py-2 mt-2 mr-4 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-gray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none focus:shadow-outline focus:ring-2 ring-offset-current ring-offset-2"name="team_name">
                        <label>team members </label> <input class="w-full px-4 py-2 mt-2 mr-4 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-gray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none focus:shadow-outline focus:ring-2 ring-offset-current ring-offset-2" name="team_members">
                        <label>attendance type </label> <br>
                        <input type="checkbox" name="att_type" value="1">  Physical<br>      
                        <input type="checkbox" name="att_type" value="2">  Virtual<br><br>
                        <label>location</label>  <input class="w-full px-4 py-2 mt-2 mr-4 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-gray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none focus:shadow-outline focus:ring-2 ring-offset-current ring-offset-2" name="location">
                        <label>province </label>  <input class="w-full px-4 py-2 mt-2 mr-4 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-gray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none focus:shadow-outline focus:ring-2 ring-offset-current ring-offset-2" name="province">
                        <div class="form-group">
                          <label for="african-countries">Country</i></label>
                          <select type="text" class="form-control " name="country">
                          <option selected>Choose</option>
                          <option value="algeria">Algeria</option>
                          <option value="angola">Angola</option>
                          <option value="benin">Benin</option>
                          <option value="botswana">Botswana</option>
                          <option value="burkina Faso">Burkina Faso</option>
                          <option value="burundi">Burundi</option>
                          <option value="cameroon">Cameroon</option>
                          <option value="cape-verde">Cape Verde</option>
                          <option value="central-african-republic">Central African Republic</option>
                          <option value="chad">Chad</option>
                          <option value="comoros">Comoros</option>
                          <option value="congo-brazzaville">Congo - Brazzaville</option>
                          <option value="congo-kinshasa">Congo - Kinshasa</option>
                          <option value="ivory-coast">Côte d’Ivoire</option>
                          <option value="djibouti">Djibouti</option>
                          <option value="egypt">Egypt</option>
                          <option value="equatorial-guinea">Equatorial Guinea</option>
                          <option value="eritrea">Eritrea</option>
                          <option value="ethiopia">Ethiopia</option>
                          <option value="gabon">Gabon</option>
                          <option value="gambia">Gambia</option>
                          <option value="ghana">Ghana</option>
                          <option value="guinea">Guinea</option>
                          <option value="guinea-bissau">Guinea-Bissau</option>
                          <option value="kenya">Kenya</option>
                          <option value="lesotho">Lesotho</option>
                          <option value="liberia">Liberia</option>
                          <option value="libya">Libya</option>
                          <option value="madagascar">Madagascar</option>
                          <option value="malawi">Malawi</option>
                          <option value="mali">Mali</option>
                          <option value="mauritania">Mauritania</option>
                          <option value="mauritius">Mauritius</option>
                          <option value="mayotte">Mayotte</option>
                          <option value="morocco">Morocco</option>
                          <option value="mozambique">Mozambique</option>
                          <option value="namibia">Namibia</option>
                          <option value="niger">Niger</option>
                          <option value="nigeria">Nigeria</option>
                          <option value="rwanda">Rwanda</option>
                          <option value="reunion">Réunion</option>
                          <option value="saint-helena">Saint Helena</option>
                          <option value="senegal">Senegal</option>
                          <option value="seychelles">Seychelles</option>
                          <option value="sierra-leone">Sierra Leone</option>
                          <option value="somalia">Somalia</option>
                          <option value="south-africa">South Africa</option>
                          <option value="sudan">Sudan</option>
                          <option value="south-sudan">Sudan</option>
                          <option value="swaziland">Swaziland</option>
                          <option value="Sao-tome-príncipe">São Tomé and Príncipe</option>
                          <option value="tanzania">Tanzania</option>
                          <option value="togo">Togo</option>
                          <option value="tunisia">Tunisia</option>
                          <option value="uganda">Uganda</option>
                          <option value="western-sahara">Western Sahara</option>
                          <option value="zambia">Zambia</option>
                          <option value="zimbabwe">Zimbabwe</option>
                          </select>
                          </div>
                        <label>Describe Project / Learning to work on at Summer Dev Days </label> <input class="w-full px-4 py-2 mt-2 mr-4 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-gray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none focus:shadow-outline focus:ring-2 ring-offset-current ring-offset-2" name="confirm">
                        <label>Progress of the Project / Learning </label> <input  type="number" class="w-full px-4 py-2 mt-2 mr-4 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-gray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none focus:shadow-outline focus:ring-2 ring-offset-current ring-offset-2" name="source">
                        <label>Dev Labs Sessions to Attend  </label> <br>   
                        <input type="checkbox" name="subscribed" value="1">  Microsoft Azure<br>      
                        <input type="checkbox" name="subscribed" value="2">  Lepsta Platform<br>
                        <input type="checkbox" name="subscribed" value="2">  Javascript<br>    
                        <input type="checkbox" name="subscribed" value="2">  Mapping intelligence<br>      
                        <input type="checkbox" name="subscribed" value="3">  All thing apps<br><br>
                        <label>Why are you looking forward to Summer Dev Days? </label>  <input class="w-full px-4 py-2 mt-2 mr-4 text-base text-black transition duration-500 ease-in-out transform rounded-lg bg-gray-100 focus:border-blueGray-500 focus:bg-white focus:outline-none focus:shadow-outline focus:ring-2 ring-offset-current ring-offset-2" name="data">
                         </form>
                    </div>
                </div>
          
            </div>
        </div>
    </div>
    
</section>
