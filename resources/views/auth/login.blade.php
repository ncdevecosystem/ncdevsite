@extends('layouts.auth')

@section('content')
<h3 class="text-center mb-4">Sign In</h3>
<form method="POST" action="{{ route('login') }}">
    @csrf
    
<div class="form-group">
    <input id="email" type="email" class="form-control rounded-left @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

    @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>
<div class="form-group d-flex">
<input id="password" type="password" class="form-control rounded-left @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

    @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>
<div class="form-group">
<button type="submit" class="form-control btn btn-primary rounded submit px-3">Login</button>
</div>
<div class="form-group d-md-flex">
<div class="w-50">
<label class="checkbox-wrap checkbox-primary">Remember Me
<input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
<span class="checkmark"></span>
</label>
</div>
<div class="w-50 text-md-right">
    @if (Route::has('password.request'))
    <a  href="{{ route('password.request') }}">
        {{ __('Forgot Your Password?') }}
    </a>
@endif
</div>
</div>
</form>
@endsection
