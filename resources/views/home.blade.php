<!DOCTYPE html>
<html lang="en">

	
	<title>Ncdev Ecosystem Dashboard</title>

	<link rel="icon" href="https://ncdev.co.za/wp-content/uploads/2021/01/index2.jpg" type="image/png" />
	
	<link rel="stylesheet" href="https://unpkg.com/tailwindcss@1.9.6/dist/tailwind.min.css">
	<link href="https://unpkg.com/pattern.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
	<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/css/bootstrap.min.css" />
	<!-- themefy CSS -->
	<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/themefy_icon/themify-icons.css" />
	<!-- swiper slider CSS -->
	<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/swiper_slider/css/swiper.min.css" />
	<!-- select2 CSS -->
	<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/select2/css/select2.min.css" />
	<!-- select2 CSS -->
	<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/niceselect/css/nice-select.css" />
	<!-- owl carousel CSS -->
	<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/owl_carousel/css/owl.carousel.css" />
	<!-- gijgo css -->
	<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/gijgo/gijgo.min.css" />
	<!-- font awesome CSS -->
	<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/font_awesome/css/all.min.css" />
	<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/tagsinput/tagsinput.css" />
	<!-- datatable CSS -->
	<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/datatable/css/jquery.dataTables.min.css" />
	<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/datatable/css/responsive.dataTables.min.css" />
	<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/datatable/css/buttons.dataTables.min.css" />
	<!-- text editor css -->
	<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/text_editor/summernote-bs4.css" />
	<!-- morris css -->
	<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/morris/morris.css">
	<!-- metarial icon css -->
	<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/material_icon/material-icons.css" />
	
	<!-- menu css  -->
	<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/css/metisMenu.css">
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- style CSS -->
	<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/css/style.css" />
	<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/css/colors/default.css" id="colorSkinCSS">
	<link href="{{ asset('/css/home.css') }}" rel="stylesheet">
	
 <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>
 <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> 
	
	<!-- component -->

	<body>
		<div>
			<script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
			<div x-data="{ sidebarOpen: false }" class="flex h-screen bg-gray-200">
			<div :class="sidebarOpen ? 'block' : 'hidden'" @click="sidebarOpen = false" class="fixed z-20 inset-0 bg-black opacity-50 transition-opacity lg:hidden"></div>
			<div :class="sidebarOpen ? 'translate-x-0 ease-out' : '-translate-x-full ease-in'" class="fixed z-30 inset-y-0 left-0 w-64 transition duration-300 transform bg-gray-900 overflow-y-auto lg:translate-x-0 lg:static lg:inset-0">
			<div class="flex items-center justify-center mt-8">
			<div class="flex items-center">
			<i class="fa fa-map-signs text-white"></i>
			<span class="text-white text-2xl mx-2 font-semibold">Ncdev Ecosystem</span>
			</div>
			</div>
			<nav class="mt-10">
			<a class="flex items-center mt-4 py-2 px-6 text-gray-500 hover:bg-gray-700 hover:bg-opacity-25 hover:text-gray-100 text-gray-100" href="/home"><i class="fa fa-user"></i>
			<span class="mx-3"> Dashboard</span></a>
			
			<a class="flex items-center mt-4 py-2 px-6 text-gray-500 hover:bg-gray-700 hover:bg-opacity-25 hover:text-gray-100" href="/blog_admin">
			<i class="fa fa-tags"></i><span class="mx-3"> Blog Editor</span></a>
			
			<a class="flex items-center mt-4 py-2 px-6 text-gray-500 hover:bg-gray-700 hover:bg-opacity-25 hover:text-gray-100"
			href="/admin/events/event_list"> <i class="fa fa-calendar"></i>
			<span class="mx-3"> Events Manager</span>
			</a>
			
			<a class="flex items-center mt-4 py-2 px-6 text-gray-500 hover:bg-gray-700 hover:bg-opacity-25 hover:text-gray-100"
			href="/laravel-filemanager#"> <i class="fa fa-folder"></i>
			<span class="mx-3"> File Manager</span>
			</a>
			
			<a class="flex items-center mt-4 py-2 px-6 text-gray-500 hover:bg-gray-700 hover:bg-opacity-25 hover:text-gray-100"
			href="/dashboard/media"> <i class="fa fa-group"></i>
			<span class="mx-3"> Community</span>
			</a>
			
			<a class="flex items-center mt-4 py-2 px-6 text-gray-500 hover:bg-gray-700 hover:bg-opacity-25 hover:text-gray-100"
			href="/dashboard/short-link"><i class="fa fa-link"></i>
			<span class="mx-3">VacWork</span>
			</a>
			<a class="flex items-center mt-4 py-2 px-6 text-gray-500 hover:bg-gray-700 hover:bg-opacity-25 hover:text-gray-100"
			href="/dashboard/assets"> <i class="fa fa-cogs"></i>
			<span class="mx-3">Settings</span>
			</a>
			</nav>
			</div>
			<div class="flex-1 flex flex-col overflow-hidden">
			<header class="flex justify-between items-center py-4 px-6 bg-white border-b-4 border-red-700">
			<div class="flex items-center">
			<button @click="sidebarOpen = true" class="text-gray-500 focus:outline-none lg:hidden">
			<svg class="h-6 w-6" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path d="M4 6H20M4 12H20M4 18H11" stroke="currentColor" stroke-width="2" stroke-linecap="round"
			stroke-linejoin="round"></path>
			</svg>
			</button>
			<div class="relative mx-4 lg:mx-0">
			<span class="absolute inset-y-0 left-0 pl-3 flex items-center">
			<svg class="h-5 w-5 text-gray-500" viewBox="0 0 24 24" fill="none">
			<path
			d="M21 21L15 15M17 10C17 13.866 13.866 17 10 17C6.13401 17 3 13.866 3 10C3 6.13401 6.13401 3 10 3C13.866 3 17 6.13401 17 10Z"
			stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
			</path>
			</svg>
			</span>
			<input class="form-input w-32 sm:w-64 rounded-md pl-10 pr-4 focus:border-red-600" type="text"
			placeholder="Search">
			</div>
			</div>
			<div class="flex items-center">
			<div x-data="{ notificationOpen: false }" class="relative flex">
			<a href="/dashboard/crew/edit/6"
			class="block px-2 text-sm text-white hover:bg-red-600 hover:text-white btn btn-danger mx-1"><i class="fa fa-edit"></i> Edit Profile</a>
			
			<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="block px-2 text-sm text-white hover:bg-red-600 hover:text-white btn btn-danger mx-1">Log Out <i class="fa fa-arrow-right"></i>
			  <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
				  @csrf
			  </form>
			</a>
			</div>
			</header>
			<main class="flex-1 overflow-x-hidden overflow-y-auto bg-gray-200">
			<div class="mx-auto px-6 py-8">
			  <section>
				<div >
					<div class="container-fluid">
						<div class="row">
							<div class="col-lg-3 col-xs-6">
								<div class="rad-info-box rad-txt-primary">
									<i class="fa fa-calendar "></i>
									<span class="heading">Events</span>
									<span class="value"><span>{{ $event->count() }}</span></span>
								</div>
							</div>
				  <div class="col-lg-3 col-xs-6">
								<div class="rad-info-box rad-txt-success">
									<i class="fa fa-clipboard "></i>
									<span class="heading">Posts</span>
									<span class="value"><span>{{ $post->count() }}</span></span>
								</div>
							</div>
							<div class="col-lg-3 col-xs-6">
								<div class="rad-info-box rad-txt-danger">
									<i class="fa fa-users"></i>
									<span class="heading">Communties</span>
									<span class="value"><span>0</span></span>
								</div>
							</div>
							<div class="col-lg-3 col-xs-6">
								<div class="rad-info-box">
									<i class="fa fa-book"></i>
									<span class="heading">Vacworks</span>
									<span class="value"><span>0</span></span>
								</div>
							</div>
							<div class="col-lg-3 col-xs-6">
								<div class="rad-info-box">
									<i class="fa fa-video-camera"></i>
									<span class="heading">Virtuals Events</span>
									<span class="value"><span>0</span></span>
								</div>
							</div>
							<div class="col-lg-3 col-xs-6">
								<div class="rad-info-box rad-txt-danger">
									<i class="fa fa-user"></i>
									<span class="heading">Physical Events</span>
									<span class="value"><span>0</span></span>
								</div>
							</div>
							<div class="col-lg-3 col-xs-6">
								<div class="rad-info-box rad-txt-success">
									<i class="fa fa-list"></i>
									<span class="heading">Attends</span>
									<span class="value"><span>{{ $booking->count() }}</span></span>
								</div>
							</div>
							<div class="col-lg-3 col-xs-6">
								<div class="rad-info-box  rad-txt-primary">
									<i class="fa fa-list"></i>
									<span class="heading">Attends</span>
									<span class="value"><span>0</span></span>
								</div>
							</div>
						</div>
							</div>
							<br>
							<br>
							<br>
							<div class="chart-container">
								<div class="pie-chart-container">
								  <canvas id="pie-chart"></canvas>
								</div>
							  </div>
						</div>
					</div>
				</div>
				
			  </section>
			</div>
			
			</main>
			  <script>
				$(function(){
					//get the pie chart canvas
					var cData = JSON.parse(`<?php echo $chart_data; ?>`);
					var ctx = $("#pie-chart");
			  
					//pie chart data
					var data = {
					  labels: cData.label,
					  datasets: [
						{
						  label: "Users Count",
						  data: cData.data,
						  backgroundColor: [
							"#DEB887",
							"#A9A9A9",
							"#DC143C",
							"#F4A460",
							"#2E8B57",
							"#1D7A46",
							"#CDA776",
						  ],
						  borderColor: [
							"#CDA776",
							"#989898",
							"#CB252B",
							"#E39371",
							"#1D7A46",
							"#F4A460",
							"#CDA776",
						  ],
						  borderWidth: [1, 1, 1, 1, 1,1,1]
						}
					  ]
					};
			  
					//options
					var options = {
					  responsive: true,
					  title: {
						display: true,
						position: "top",
						text: "Last Week Event Bookings -  Day Wise Count",
						fontSize: 18,
						fontColor: "#111"
					  },
					  legend: {
						display: true,
						position: "bottom",
						labels: {
						  fontColor: "#333",
						  fontSize: 16
						}
					  }
					};
			  
					//create Pie Chart class object
					var chart1 = new Chart(ctx, {
					  type: "pie",
					  data: data,
					  options: options
					});
			  
				});
			  </script>	  
	</body>
</html>