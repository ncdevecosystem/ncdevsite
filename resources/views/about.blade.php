
   @extends('layouts.app')

   @section('content')

<link rel="stylesheet" href="https://tailwindcomponents.com/css/component.landing-page-example.css">

<script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>


    <header x-data="{ isOpen: false }">
        <img src="images/about.jpg" alt="">
      
    </header>

    {{-- about --}}
<!-- component -->
<section class="py-20 bg-stone-100 my-20" >
  <div class="grid grid-cols-1 sm:grid-cols-2  gap-10 mx-auto  items-center max-w-6xl mx-auto">
    <div class="px-4 md:mr-6">
      <h4 class="font-size38 " style="font-size: 38px; color:#6a6a6a" >
        With our diverse range of lenders, we fight hard to get you the best deal.
      </h4>
      <p class="mt-4 text-stone-800 text-xl font-medium">
        With our diverse range of lenders, we fight hard to get you the best deal.
         Lorem ipsum dolor sit amet consectetur adipisicing elit.
          Molestiae quos magnam quibusdam cupiditate, fuga id quidem nam
           sequi eius animi, quo exercitationem!
         Libero fugiat perferendis illum amet minus qui nisi?
      </p>
    </div>
    <div>
      <div class="absolute bg-blue-500 transform -translate-x-10 relative h-64" style="background-color: brown">
      </div>
      <div class="transform md:rounded-md rotate-3 scale-110 translate-x-10 md:shadow-2xl -ml-4 -mt-44 p-12  space-y-2" style="background-color: #faf4f4">
        
                      <img src="https://live.staticflickr.com/1864/43564597335_c995ce1a3a_c_d.jpg">
                    
      </div>
    </div>
  </div>
</section>
    {{-- end about --}}
    {{-- events section --}}
   
<div class="2xl:container 2xl:mx-auto md:py-12 lg:px-20 md:px-6 py-9 px-4" style="background: #fafafa">

  <div class="text-center">
    <div role="contentinfo" class="flex items-center flex-col px-4">
                   
      <h4 class="font-size38 " style="font-size: 38px; color:#6a6a6a" ><b>About NCDev Ecosystem</b></h4>
  </div>
    <p class="font-normal text-base leading-6 dark:text-gray-400 text-gray-600 mt-4 lg:w-5/12 md:w-9/12 mx-auto">Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci quas consectetur velit itaque expedita eum. Reiciendis tempora beatae, totam harum corrupti quas quod atque
       culpa delectus recusandae? Id, consequatur facilis! Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum quam dolorem iure soluta aliquid, ipsa impedit veniam, repudiandae, sapiente dolor numquam neque illo! Mollitia corrupti 
       voluptatibus </p>
  </div>
  <div class="grid lg:grid-cols-4 sm:grid-cols-2 grid-cols-1 lg:grap-8 md:gap-6 gap-4 mt-10">
    <div class="relative group">
      <img src="https://live.staticflickr.com/65535/47974745196_8f44a4561f_k_d.jpg" alt="A picture of a sitting dog" class="lg:block hidden w-full" />
      <img src="https://live.staticflickr.com/65535/47970983853_49de3ae2f5_c_d.jpg" alt="A picture of a sitting dog" class="lg:hidden block w-full" />
      <div class="flex justify-center items-center opacity-0 bg-gradient-to-t from-gray-800 via-gray-800 to-opacity-30 group-hover:opacity-50 absolute top-0 left-0 h-full w-full"></div>
      
    </div>
    <div class="relative group">
      <img src="https://live.staticflickr.com/65535/47970983853_49de3ae2f5_c_d.jpg" alt="Smiling Girl" class="lg:block hidden w-full" />
      <img src="https://i.ibb.co/YD8nNMR/pexels-leah-kelley-1449667-1-1.png" alt="Smiling Girl" class="lg:hidden block w-full" />
      <div class="opacity-0 bg-gradient-to-t from-gray-800 via-gray-800 to-opacity-30 group-hover:opacity-50 absolute top-0 left-0 h-full w-full"></div>
     
    </div>
    <div class="relative group">
      <img src="https://live.staticflickr.com/65535/47971201221_3ec62ae967_c_d.jpg" alt="Men Posing" class="lg:block hidden w-full" />
      <img src="https://i.ibb.co/myWxfSm/pexels-spencer-selover-775358-1-1.png" alt="Men Posing" class="lg:hidden block w-full" />
      <div class="opacity-0 bg-gradient-to-t from-gray-800 via-gray-800 to-opacity-30 group-hover:opacity-50 absolute top-0 left-0 h-full w-full"></div>
      
    </div>
    <div class="relative group">
      <img src="https://live.staticflickr.com/65535/40930266253_a6b71c1409_c_d.jpg" alt="2 puppies" class="lg:block hidden w-full" />
      <img src="https://i.ibb.co/5cDQZ2r/pexels-chevanon-photography-1108099-1-1.png" alt="2 puppies" class="lg:hidden block w-full" />
      <div class="opacity-0 bg-gradient-to-t from-gray-800 via-gray-800 to-opacity-30 group-hover:opacity-50 absolute top-0 left-0 h-full w-full"></div>
     
    </div>
  </div>
</div>
    {{-- end section --}}
    
</div>

{{-- section --}}
{{-- <section class="bg-white" style="background-color:black">
  <div class="max-w-5xl px-6 py-16 mx-auto">
      
      <div class="grid gap-8 mt-10 md:grid-cols-2 lg:grid-cols-3">
          <div class="px-6 py-8 overflow-hidden bg-white rounded-md shadow-md" style="background-color:white">
            <h4 class="font-size38 " style="font-size: 25px; color:rgb(42, 42, 42)" ><b>Our Mission</b></h4>
              <p class="max-w-md mt-4 text-gray-700">Lorem ipsum dolor sit amet, consectetur adipiscing Ac aliquam ac
                  volutpat, viverra magna risus aliquam massa.</p>
          </div>

          <div class="px-6 py-8 overflow-hidden bg-white rounded-md shadow-md" style="background-color:white">
            <h4 class="font-size38 " style="font-size: 25px; color:#6a6a6a" ><b>Our Vision</b></h4>
              <p class="max-w-md mt-4 text-gray-700">Lorem ipsum dolor sit amet, consectetur adipiscing Ac aliquam ac
                  volutpat,
                  viverra magna risus aliquam massa.</p>
          </div>

          <div class="px-6 py-8 overflow-hidden bg-white rounded-md shadow-md" style="background-color:#white">
            <h4 class="font-size38 " style="font-size: 25px; color:#6a6a6a" ><b>Our Purpose</b></h4>
              <p class="max-w-md mt-4 text-gray-700">Lorem ipsum dolor sit amet, consectetur adipiscing Ac aliquam ac
                  volutpat,
                  viverra magna risus aliquam massa.</p>
          </div>
      </div>
  </div>
</section> --}}
{{-- end section --}}

<div class="2xl:container 2xl:mx-auto md:py-12 lg:px-20 md:px-6 py-9 px-4">
 
  <div id="viewerButton" class="hidden w-full flex justify-center">
    <button onclick="openView()" class="bg-white text-indigo-600 shadow-md rounded focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-600 py-5 px-10 font-semibold">Open Quick View</button>
  </div>
  <div id="viewerBox" class="lg:p-10 md:p-6 p-4 bg-white dark:bg-gray-900">
    
    <div class="mt-3 md:mt-4 lg:mt-0 flex flex-col lg:flex-row items-strech justify-center lg:space-x-8">
      <div class="lg:w-1/2 flex justify-between items-strech bg-gray-50  px-2 py-20 md:py-6 md:px-6 lg:py-24" style="background: black">
       
        <div >
          <img src="https://live.staticflickr.com/65535/47970983853_49de3ae2f5_c_d.jpg" alt="">
        </div>
       
      </div>
      <div class="lg:w-1/2 flex flex-col justify-center mt-7 md:mt-8 lg:mt-0 pb-8 lg:pb-0">
        <h4 class="font-size38 " style="font-size: 38px; color:#6a6a6a" >Our Main Focus</h4>
        <p class="text-base leading-normal text-gray-600 dark:text-white mt-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam architecto deserunt tempora dolorem expedita repudiandae debitis quidem ipsa facilis, provident nisi maiores quisquam. Deleniti vero quia non sed. Quis, aliquid.
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. 
          Error ratione iure eligendi assumenda. Iusto ipsam pariatur 
          accusamus magni nam doloremque optio consequuntur nobis.
           Vero, molestias dolorem modi suscipit quas quam.
        </p>
        <p class="text-3xl font-medium text-gray-600 dark:text-white mt-8 md:mt-10"></p>
        <div class="flex items-center flex-col md:flex-row space-y-4 md:space-y-0 md:space-x-6 lg:space-x-8 mt-8 md:mt-16">
          <button class="w-full md:w-3/5 border border-gray-800 text-base font-medium leading-none text-white uppercase py-6 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-800 bg-red-800 text-white dark:bg-white dark:text-brown-500 dark:hover:bg-black">Get in touch</button>
        </div>

      </div>
    </div>
  </div>
</div>
<style>.slider {
  width: 200px;
  height: 400px;
  position: relative;
  overflow: hidden;
}

.slide-ana {
  height: 360px;
}

.slide-ana > div {
  width: 100%;
  height: 100%;
  position: absolute;
  transition: all 0.7s;
}

@media (min-width: 200px) and (max-width: 639px) {
  .slider {
    height: 300px;
    width: 170px;
  }
}
</style>
<script>// more free and premium Tailwind CSS components at https://tailwinduikit.com/
let slides = document.querySelectorAll(".slide-ana>div");
let slideSayisi = slides.length;
let prev = document.getElementById("prev");
let next = document.getElementById("next");
for (let index = 0; index < slides.length; index++) {
  const element = slides[index];
  element.style.transform = "translateX(" + 100 * index + "%)";
}
let loop = 0 + 1000 * slideSayisi;

function goNext() {
  loop++;
  for (let index = 0; index < slides.length; index++) {
    const element = slides[index];
    element.style.transform =
      "translateX(" + 100 * (index - (loop % slideSayisi)) + "%)";
  }
}

function goPrev() {
  loop--;
  for (let index = 0; index < slides.length; index++) {
    const element = slides[index];
    element.style.transform =
      "translateX(" + 100 * (index - (loop % slideSayisi)) + "%)";
  }
}

function openView() {
  document.getElementById("viewerButton").classList.add("hidden");
  document.getElementById("viewerBox").classList.remove("hidden");
}
function closeView() {
  document.getElementById("viewerBox").classList.add("hidden");
  document.getElementById("viewerButton").classList.remove("hidden");
}
</script>

 

@endsection
