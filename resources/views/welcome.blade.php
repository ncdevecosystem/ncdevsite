@extends('layouts.app')

@section('content')

<header x-data="{ isOpen: false }">
  <img src="images/banner.jpg" alt="">
 
 
</header>
{{-- overview --}}

<div class="py-16">
<div class="container m-auto px-6">

<div class="lg:flex justify-between items-center">
<div class="lg:w-6/12 lg:p-0 p-7">
  <h1 class="text-4xl leading-tight mb-5 capitalize" style="font-size: 38px; color:#6a6a6a">An overview of NCDev Ecosytem. </h1>
  <p style="font-size: 15px" class="mt-4 text-lg leading-relaxed text-blueGray-500">  With Tailwind you can optimized the customization process to save your team time when building websites. 
    With Tailwind you can optimized the customization process to save your team time when building websites.
    With Tailwind you can optimized the customization process to save your team time when building websites.
    With Tailwind you can optimized the customization process to save your team time when building websites.
    With Tailwind you can optimized the customization process to save your team time when building websites.
    
  </p>


</div>
<div class="lg:w-5/12 order-2">
  <img style="filter: grayscale(1);" src="https://live.staticflickr.com/1864/43564597335_c995ce1a3a_c_d.jpg"
  style="transform: scale(1) perspective(1040px) rotateY(-11deg) rotateX(2deg) rotate(2deg);" alt="" class="rounded">
</div>
</div>

</div>
</div>
{{-- end overview --}}
{{-- Section Initiatives--}}

<section class="text-gray-900 body-font" style="background: #fafafa; border-bottom: 2px solid rgb(185, 176, 176);border-top: 2px solid rgb(185, 176, 176);">
  <div class="container px-5 py-24 mx-auto">
    <div class="flex flex-wrap w-full mb-8">
      <div class="w-full mb-6 lg:mb-0">
       
        <div class="h-1 w-20 bg-red-500 rounded col-lg-12 m-auto text-center "></div><br><br>
      </div>
    </div>
    <div class="flex flex-wrap -m-4">
      <div class="p-4 lg:w-1/3 w-1/2">
        <div class="flex rounded-lg h-full p-8 flex-col" style="background: rgb(42, 42, 42)">
          <div class="flex items-center mb-3">
            <div class="w-8 h-8 mr-3 inline-flex items-center justify-center rounded-full bg-red-400 text-white flex-shrink-0">
              <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
                <path d="M22 12h-4l-3 9L9 3l-3 9H2"></path>
              </svg>
            </div>
            <h2 class="text-white text-lg title-font font-medium">Initiave1</h2>
          </div>
          <div class="flex-grow">
            <p class="leading-relaxed text-base text-white">Microsoft Azure, often referred to as Azure, is a cloud computing service operated by Microsoft for application management via Microsoft-managed data centers.</p>
            <a href="https://azure.microsoft.com/en-us/" class="mt-3 text-gray-100 hover:text-gray-100 inline-flex items-center">Learn More
              <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-4 h-4 ml-2" viewBox="0 0 24 24">
                <path d="M5 12h14M12 5l7 7-7 7"></path>
              </svg>
            </a>
          </div>
        </div>
      </div>
      <div class="p-4 lg:w-1/3 w-1/2">
        <div class="flex rounded-lg h-full  p-8 flex-col" style="background: rgb(42, 42, 42)">
          <div class="flex items-center mb-3">
            <div class="w-8 h-8 mr-3 inline-flex items-center justify-center rounded-full bg-red-400 text-white flex-shrink-0">
              <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
                <path d="M20 21v-2a4 4 0 00-4-4H8a4 4 0 00-4 4v2"></path>
                <circle cx="12" cy="7" r="4"></circle>
              </svg>
            </div>
            <h2 class="text-white text-lg title-font font-medium">Initative2</h2>
          </div>
          <div class="flex-grow">
            <p class="leading-relaxed text-base text-white">Lepsta is a software development platform that helps developers produce high-quality software at a fast rate.</p>
            <a href="https://lepsta.tech/home/" class="mt-3 text-gray-100 hover:text-gray-100 inline-flex items-center">Learn More
              <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-4 h-4 ml-2" viewBox="0 0 24 24">
                <path d="M5 12h14M12 5l7 7-7 7"></path>
              </svg>
            </a>
          </div>
        </div>
      </div>
      <div class="p-4 lg:w-1/3 w-1/2">
        <div class="flex rounded-lg h-full  p-8 flex-col" style="background: rgb(42, 42, 42)">
          <div class="flex items-center mb-3">
            <div class="w-8 h-8 mr-3 inline-flex items-center justify-center rounded-full bg-red-400 text-white flex-shrink-0">
              <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
                <circle cx="6" cy="6" r="3"></circle>
                <circle cx="6" cy="18" r="3"></circle>
                <path d="M20 4L8.12 15.88M14.47 14.48L20 20M8.12 8.12L12 12"></path>
              </svg>
            </div>
            <h2 class="text-white text-lg title-font font-medium">Initative2</h2>
          </div>
          <div class="flex-grow">
            <p class="leading-relaxed text-base text-white">All things Apps including the Front-End, Back-End and technologies used for apps.</p>

              <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-4 h-4 ml-2" viewBox="0 0 24 24">
                <path d="M5 12h14M12 5l7 7-7 7"></path>
              </svg>
            </a>
          </div>
        </div>
      </div>
      <div class="p-4 lg:w-1/3 w-1/2">
        <div class="flex rounded-lg h-full p-8 flex-col" style="background: rgb(42, 42, 42)">
          <div class="flex items-center mb-3">
            <div class="w-8 h-8 mr-3 inline-flex items-center justify-center rounded-full bg-red-400 text-white flex-shrink-0">
              <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
                <circle cx="6" cy="6" r="3"></circle>
                <circle cx="6" cy="18" r="3"></circle>
                <path d="M20 4L8.12 15.88M14.47 14.48L20 20M8.12 8.12L12 12"></path>
              </svg>
            </div>
            <h2 class="text-white text-lg title-font font-medium">Mapping Intelligence</h2>
          </div>
          <div class="flex-grow">
            <p class="leading-relaxed text-base text-white">Map Intelligence is a fast, highly configurable and efficient platform for delivering customised, map-enabled business applications involving BI tools and mapping providers and GIS. It is designed for dynamic use in a web-browser with or without BI integration.</p>
            
              <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-4 h-4 ml-2" viewBox="0 0 24 24">
                <path d="M5 12h14M12 5l7 7-7 7"></path>
              </svg>
            </a>
          </div>
        </div>
      </div>
      <div class="p-4 lg:w-1/3 w-1/2">
        <div class="flex rounded-lg h-full p-8 flex-col" style="background: rgb(42, 42, 42)">
          <div class="flex items-center mb-3">
            <div class="w-8 h-8 mr-3 inline-flex items-center justify-center rounded-full bg-red-400 text-white flex-shrink-0">
              <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
                <path d="M20 21v-2a4 4 0 00-4-4H8a4 4 0 00-4 4v2"></path>
                <circle cx="12" cy="7" r="4"></circle>
              </svg>
            </div>
            <h2 class="text-white text-lg title-font font-medium">Lepsta Platform</h2>
          </div>
          <div class="flex-grow">
            <p class="leading-relaxed text-base text-white">Lepsta is a software development platform that helps developers produce high-quality software at a fast rate.</p>
            <a href="https://lepsta.tech/home/" class="mt-3 text-gray-100 hover:text-gray-100 inline-flex items-center">Learn More
              <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-4 h-4 ml-2" viewBox="0 0 24 24">
                <path d="M5 12h14M12 5l7 7-7 7"></path>
              </svg>
            </a>
          </div>
        </div>
      </div>

      <div class="p-4 lg:w-1/3 w-1/2">
        <div class="flex rounded-lg h-full  p-8 flex-col" style="background: rgb(42, 42, 42)">
          <div class="flex items-center mb-3">
            <div class="w-8 h-8 mr-3 inline-flex items-center justify-center rounded-full bg-red-400 text-white flex-shrink-0">
              <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-5 h-5" viewBox="0 0 24 24">
                <path d="M20 21v-2a4 4 0 00-4-4H8a4 4 0 00-4 4v2"></path>
                <circle cx="12" cy="7" r="4"></circle>
              </svg>
            </div>
            <h2 class="text-white text-lg title-font font-medium">Lepsta Platform</h2>
          </div>
          <div class="flex-grow">
            <p class="leading-relaxed text-base text-white">Lepsta is a software development platform that helps developers produce high-quality software at a fast rate.</p>
            <a href="https://lepsta.tech/home/" class="mt-3 text-gray-100 hover:text-gray-100 inline-flex items-center">Learn More
              <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-4 h-4 ml-2" viewBox="0 0 24 24">
                <path d="M5 12h14M12 5l7 7-7 7"></path>
              </svg>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section><br><br><br>


<section id="about" class="relative pt-20 pb-20 ">
<div class="items-center flex flex-wrap" >
<div class="w-full md:w-4/12 ml-auto mr-auto px-4">
<img style="width: 100%; filter: grayscale(1);" alt="..." class="max-w-full rounded-lg shadow-lg" src="https://live.staticflickr.com/65535/47974705063_a871abea73_c_d.jpg">
</div>
<div class="w-full md:w-5/12 ml-auto mr-auto px-4">
<div class="md:pr-12">
<h3 class="font-size38 " style="font-size: 38px; color:#6a6a6a" >The Start of the Ecosystem</h3>

<p style="font-size: 15px" class="mt-4 text-lg leading-relaxed text-blueGray-500">
<br>
Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam soluta, necessitatibus consequuntur iure, culpa quas porro dolorum ea accusamus, dignissimos nulla saepe rerum natus aspernatur exercitationem aliquam a alias expedita.
<br><br>
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga sed magni officia quidem laudantium consequatur vel quibusdam, illum nemo ex voluptatem optio ea, omnis deleniti architecto iusto velit! Atque, laudantium. </p>

<blockquote class="mt-4  leading-relaxed text-blueGray-500"><p>Inventore, nam. Tempora pariatur aspernatur laboriosam repellendus aliquam minus in reprehenderit, rerum culpa atque, dolorum, veritatis voluptatem cupiditate quis repellat ab cumque!</p></blockquote>

<p class="mt-4 text-lg leading-relaxed text-blueGray-500">
Lorem ipsum dolor sit amet consectetur adipisicing elit. Inventore, nam. Tempora pariatur aspernatur laboriosam repellendus aliquam minus in reprehenderit, rerum culpa atque, dolorum, veritatis voluptatem cupiditate quis repellat ab cumque!
</p>

</div>
</div>
</div>
</section>
{{-- end desc --}}

{{-- section --}}

<section id="infozone" class="pb-20 relative block bg-black" style="border-top: 5px solid red; border-bottom: 5px solid red;background: rgb(42, 42, 42)" >
<div class="container mx-auto px-4 lg:pt-24 ">
  <div class="flex flex-wrap text-center justify-center">
    <div class="w-full lg:w-6/12 px-4">
      <b><h2 class="font-size38 " style="font-size: 38px; color:white">Born in the Ecosystem</h2></b>

      <p style="color:grey;font-size:15px" class="text-lg leading-relaxed mt-4 text-blueGray-400">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas, voluptatibus, laudantium pariatur alias non eos, et rem quia doloribus rerum eveniet! Inventore expedita veritatis
        architecto sit soluta ducimus recusandae corrupti.
      </p>
    </div>
  </div>

  <div class="flex flex-wrap mt-12 justify-center">
    <div class="w-full lg:w-4/12 px-4 text-left py-4">
      <div class="text-lightBlue-300 p-3 w-12 h-12 shadow-lg rounded-full bg-white inline-flex items-center justify-center">
        <i class="fa fa-paperclip text-xl text-gray-800"></i>
      </div>

      <h6 class="font-size38 " style="font-size: 20px; color:white">
        Agency
      </h6>
      <p  style="font-size: 15px; color:grey" class="mt-2 mb-4 text-blueGray-400 text-left">
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sequi, doloremque maiores! Facilis quaerat magnam optio dolor quasi, reprehenderit sed debitis, sequi dignissimos veritatis nesciunt
         voluptatum expedita, tempore aut! Sed, fugit.


      </p>
      
    </div>

    <div class="w-full lg:w-4/12 px-4 text-left py-4">
      <div class="text-blueGray-800 p-3 w-12 h-12 shadow-lg rounded-full bg-white inline-flex items-center justify-center">
        <i class="fas fa-poll text-xl"></i>
      </div>
      <h5 class="font-size38 " style="font-size: 20px; color:white">
        Digital Media
      </h5>
      <p style="font-size: 15px; color:grey" class="mt-2 mb-4 text-blueGray-400 text-left">
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Soluta ad quo consectetur obcaecati dolorem doloremque corrupti accusantium earum, minus ea officiis unde. 
        Eos debitis odio, ipsum quo eius sapiente sit?
      </p>
      
    </div>
    <div class="w-full lg:w-4/12 px-4 text-left py-4">
      <div class="text-blueGray-800 p-3 w-12 h-12 shadow-lg rounded-full bg-white inline-flex items-center justify-center">
        <i class="fas fa-lightbulb text-xl"></i>
      </div>
      <h5 class="font-size38 " style="font-size: 20px; color:white">Dev House</h5>
      <p style="font-size: 15px; color:grey" class="mt-2 mb-4 text-blueGray-400 text-left">
      Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dicta, cumque atque ad soluta, saepe quas natus alias nam magnam perferendis neque, et reiciendis veniam
      tempore dolores vitae magni id temporibus.
      </p>
     
    </div>
  </div>
</div>
</section>
{{-- end section --}}
<div class="pb-16" style="font-family: 'Lato', sans-serif">


<section class="max-w-8xl mx-auto container bg-white pt-16">
    <div>
        <div role="contentinfo" class="flex items-center flex-col px-4">
           
            <h4 class="font-size38 " style="font-size: 38px; color:#6a6a6a" ><b>Our Initiatives</b></h4>
        </div>
        <div tabindex="0" aria-label="group of cards" class="focus:outline-none mt-20 flex flex-wrap justify-center gap-10 px-4">
            <div tabindex="0" aria-label="card 1" class="focus:outline-none flex sm:w-full md:w-5/12 pb-20">
                <div class="w-20 h-20 relative mr-5">
                    <div class="absolute top-0 right-0 bg-indigo-100 rounded w-16 h-16 mt-2 mr-1"></div>
                    <div class="absolute text-white bottom-0 left-0 bg-red-600 rounded w-16 h-16 flex items-center justify-center mt-2 mr-3">
                        <img src="https://tuk-cdn.s3.amazonaws.com/can-uploader/icon_and_text-SVG1.svg" alt="drawer">
                    </div>
                </div>
                <div class="w-10/12">
                    <h2 tabindex="0" class="focus:outline-none text-lg font-bold leading-tight text-gray-800" style= "color:#6a6a6a">NCDev Hack</h2>
                    <p tabindex="0" class="focus:outline-none text-base text-gray-600 leading-normal pt-2">It provides a very simple start, no need to write a lot of code, you just import it and start the primitive components and create the ones you need.</p>
                </div>
            </div>
            <div tabindex="0" aria-label="card 2" class="focus:outline-none flex sm:w-full md:w-5/12 pb-20">
                <div class="w-20 h-20 relative mr-5">
                    <div class="absolute top-0 right-0 bg-indigo-100 rounded w-16 h-16 mt-2 mr-1"></div>
                    <div class="absolute text-white bottom-0 left-0 bg-red-600 rounded w-16 h-16 flex items-center justify-center mt-2 mr-3">
                        <img src="https://tuk-cdn.s3.amazonaws.com/can-uploader/icon_and_text-SVG2.svg" alt="check">
                    </div>
                </div>
                <div class="w-10/12">
                    <h2 tabindex="0" class="focus:outline-none text-lg font-semibold leading-tight text-gray-800" style= "color:#6a6a6a">NCDev Women Summit</h2>
                    <p tabindex="0" class="focus:outline-none text-base text-gray-600 leading-normal pt-2">Modify the visual appearance of your site – including colors, fonts, margins and other style-related properties – with a sophisticated style.</p>
                </div>
            </div>
            <div tabindex="0" aria-label="card 3" class="focus:outline-none flex sm:w-full md:w-5/12 pb-20">
                <div class="w-20 h-20 relative mr-5">
                    <div class="absolute top-0 right-0 bg-indigo-100 rounded w-16 h-16 mt-2 mr-1"></div>
                    <div class="absolute text-white bottom-0 left-0 bg-red-600 rounded w-16 h-16 flex items-center justify-center mt-2 mr-3">
                        <img src="https://tuk-cdn.s3.amazonaws.com/can-uploader/icon_and_text-SVG3.svg" alt="html tag">
                    </div>
                </div>
                <div class="w-10/12">
                    <h2 tabindex="0" class="focus:outline-none text-lg font-semibold leading-tight text-gray-800" style= "color:#6a6a6a">NCDev Community</h2>
                    <p tabindex="0" class="focus:outline-none text-base text-gray-600 leading-normal pt-2">Instead of just giving you the tools to create your own site, they offer you a list of themes you can choose from. Thus a handy product.</p>
                </div>
            </div>
            <div tabindex="0" aria-label="card 4" class="focus:outline-none flex sm:w-full md:w-5/12 pb-20">
                <div class="w-20 h-20 relative mr-5">
                    <div class="absolute top-0 right-0 bg-indigo-100 rounded w-16 h-16 mt-2 mr-1"></div>
                    <div class="absolute text-white bottom-0 left-0 bg-red-600 rounded w-16 h-16 flex items-center justify-center mt-2 mr-3">
                        <img src="https://tuk-cdn.s3.amazonaws.com/can-uploader/icon_and_text-SVG4.svg" alt="monitor">
                    </div>
                </div>
                <div class="w-10/12">
                    <h2 tabindex="0" class="focus:outline-none text-lg font-semibold leading-tight text-gray-800" style= "color:#6a6a6a">NCDev Academy</h2>
                    <p tabindex="0" class="focus:outline-none text-base text-gray-600 leading-normal pt-2">We have chosen the bright color palettes that arouse the only positive emotions. The kit that simply assures to be loved by everyone.</p>
                </div>
            </div>

            <div tabindex="0" aria-label="card 2" class="focus:outline-none flex sm:w-full md:w-5/12 pb-20">
              <div class="w-20 h-20 relative mr-5">
                  <div class="absolute top-0 right-0 bg-indigo-100 rounded w-16 h-16 mt-2 mr-1"></div>
                  <div class="absolute text-white bottom-0 left-0 bg-red-600 rounded w-16 h-16 flex items-center justify-center mt-2 mr-3">
                      <img src="https://tuk-cdn.s3.amazonaws.com/can-uploader/icon_and_text-SVG2.svg" alt="check">
                  </div>
              </div>
              <div class="w-10/12">
                  <h2 tabindex="0" class="focus:outline-none text-lg font-semibold leading-tight text-gray-800" style= "color:#6a6a6a">NCDev StakeHacks</h2>
                  <p tabindex="0" class="focus:outline-none text-base text-gray-600 leading-normal pt-2">Modify the visual appearance of your site – including colors, fonts, margins and other style-related properties – with a sophisticated style.</p>
              </div>
          </div>

          <div tabindex="0" aria-label="card 4" class="focus:outline-none flex sm:w-full md:w-5/12 pb-20">
            <div class="w-20 h-20 relative mr-5">
                <div class="absolute top-0 right-0 bg-indigo-100 rounded w-16 h-16 mt-2 mr-1"></div>
                <div class="absolute text-white bottom-0 left-0 bg-red-600 rounded w-16 h-16 flex items-center justify-center mt-2 mr-3">
                    <img src="https://tuk-cdn.s3.amazonaws.com/can-uploader/icon_and_text-SVG4.svg" alt="monitor">
                </div>
            </div>
            <div class="w-10/12">
                <h2 tabindex="0" class="focus:outline-none text-lg font-semibold leading-tight text-gray-800" style= "color:#6a6a6a">NCDev Open Data</h2>
                <p tabindex="0" class="focus:outline-none text-base text-gray-600 leading-normal pt-2">We have chosen the bright color palettes that arouse the only positive emotions. The kit that simply assures to be loved by everyone.</p>
            </div>
        </div>
        </div>
    </div>
</section>
</dh-component>

</div>


@endsection
