
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <link rel="icon" href="https://ncdev.co.za/wp-content/uploads/2021/01/index2.jpg" type="image/png" />
        <title>{{$event->event}}</title>

        <link rel="stylesheet" href="https://jthemes.net/themes/html/neondir/assets/css/all.min.css">
        <link rel="stylesheet" href="https://jthemes.net/themes/html/neondir/assets/css/animate.min.css">
        <link rel="stylesheet" href="https://jthemes.net/themes/html/neondir/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://jthemes.net/themes/html/neondir/assets/css/bootstrap-select.min.css">
        <link rel="stylesheet" href="https://jthemes.net/themes/html/neondir/assets/css/jquery.fancybox.min.css">
        <link rel="stylesheet" href="https://jthemes.net/themes/html/neondir/assets/css/perfect-scrollbar.css">
        <link rel="stylesheet" href="https://jthemes.net/themes/html/neondir/assets/css/slick.css">
        <link rel="stylesheet" href="https://jthemes.net/themes/html/neondir/assets/css/style.css">
        <link rel="stylesheet" href="https://jthemes.net/themes/html/neondir/assets/css/responsive.css">

        <!-- Gradient Color Scheme -->
        <link rel="stylesheet" href="https://jthemes.net/themes/html/neondir/assets/css/colors/color.css" title="color">
        <link rel="alternate stylesheet" href="https://jthemes.net/themes/html/neondir/assets/css/colors/color2.css" title="color2"> <!-- Color2 -->
        <link rel="alternate stylesheet" href="https://jthemes.net/themes/html/neondir/assets/css/colors/color3.css" title="color3"> <!-- Color3 -->
        <link rel="alternate stylesheet" href="https://jthemes.net/themes/html/neondir/assets/css/colors/color4.css" title="color4"> <!-- Color4 -->
        <link rel="alternate stylesheet" href="https://jthemes.net/themes/html/neondir/assets/css/colors/color5.css" title="color5"> <!-- Color5 -->

        <!-- Solid Color Scheme -->
        <link rel="alternate stylesheet" href="https://jthemes.net/themes/html/neondir/assets/css/colors/color6.css" title="color6"> <!-- Color6 -->
        <link rel="alternate stylesheet" href="https://jthemes.net/themes/html/neondir/assets/css/colors/color7.css" title="color7"> <!-- Color7 -->
        <link rel="alternate stylesheet" href="https://jthemes.net/themes/html/neondir/assets/css/colors/color8.css" title="color8"> <!-- Color8 -->
        <link rel="alternate stylesheet" href="https://jthemes.net/themes/html/neondir/assets/css/colors/color9.css" title="color9"> <!-- Color9 -->
        <link rel="alternate stylesheet" href="https://jthemes.net/themes/html/neondir/assets/css/colors/color10.css" title="color10"> <!-- Color10 -->

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <main>
           
            <section>
                <div class="w-100 position-relative">
                    <div class="feat-wrap2 w-100 position-relative">
                        <div class="feat-caro2">
                           
                            <div class="feat-item"><div class="feat-img" style="background-image: url(/uploads/avatars/{{ $event->image }});"></div></div>
                        </div>
                       
                    </div><!-- Featured Wrap 2 -->
                </div>
            </section>
            <section>
                <div class="w-100 pb-120 gray-bg position-relative">
                    <div class="container">
                        <div class="event-detail-wrap2 w-100">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-lg-8">
                                    <div class="event-detail-inner2 bg-white w-100 overlap205">
                                        <div class="event-detail-info2 w-100">
                                            <h2 class="mb-0">{{$event->event}}</h2>
                                            <span class="d-inline-block thm-clr">{{ \Carbon\Carbon::parse($event->starting)->format('d') }}-{{ \Carbon\Carbon::parse($event->ending)->format('d F Y ') }}</span><i class="d-inline-block">{{$event->venue}}</i>
                                            <ul class="event-detail-list mb-0 list-unstyled w-100">
                                                <li>Location:<span>{{$event->venue}}</span></li>
                                                <li>Review:<span><span class="rate text-color2"><i class="{{$event->total_view =='20' ? 'fa fa-star checked ' : 'fa fa-star'}}"></i><i class="{{$event->total_view =='40' ? 'fa fa-star ' : 'fa fa-star checked'}}"></i><i class="{{$event->total_view =='60' ? 'fa fa-star ' : 'fa fa-star checked'}}"></i><i class="{{$event->total_view =='80' ? 'fa fa-star ' : 'fa fa-star checked'}}"></i><i class="{{$event->total_view =='100' ? 'fa fa-star ' : 'fa fa-star checked'}}"></i></span>{{$views->total_views}} reviews</span></li>
                                                <li>Phone:<span>(+0064) 725 4143 68</span></li>
                                                <li>Website:<span><a href="{{$event->url}}">{{$event->url}}</a></span></li>
                                            </ul>
                                            <div class="reviewer-review-btns d-inline-flex align-items-center w-100">
                                                <a class="share-btn" href="" title="">Share</a>
                                                <a class="thm-btn" href="/booking/{{$event->id}}" title="">Join Event</a>
                                            </div>
                                        </div>
                                        <div class="event-detail-content-inner w-100">
                                            <h3>About Event</h3>
                                            <p class="mb-0"> {!! $event->description !!}</p>
                                           
                                        </div>
                                        <div class="event-detail-content-inner w-100">
                                            <h3>Who Speaking?</h3>
                                            <div class="speaker-wrap w-100">
                                                <div class="row">
                                                    @foreach ($speakers as $item)
                                                    <div class="col-md-4 col-sm-4 col-lg-4">
                                                        <div class="speaker-box text-center w-100">
                                                            <img class="img-fluid rounded-circle" src="/speakers/{{ $item->image }}" alt="Speaker Image 1">
                                                            <h4 class="mb-0">{{$item->fullname}}</h4>
                                                            <span class="d-block">{{$item->company}}</span>
                                                        </div>
                                                    </div> 
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div class="event-detail-content-inner w-100">
                                            <h3>Location</h3>
                                            <div class="listing-loc-map place-map" id="listing-loc-map"></div>
                                            <ul class="location-add-list mb-0 list-unstyled d-flex flex-wrap">
                                                <li><span><i class="rounded-circle fa fa-map-marker"></i>484 Ellis St, San Francisco, CA 94102, United States</span></li>
                                                <li><span><i class="rounded-circle fa fa-phone"></i>+61 2 8236 9200</span></li>
                                                <li><span><i class="rounded-circle fa fa-envelope"></i>youremail@mail.com</span></li>
                                                <li><span><i class="rounded-circle fa fa-globe"></i>www.website.com</span></li>
                                            </ul>
                                        </div>
                                        <div class="event-detail-content-inner w-100">
                                            <h3>Our Sponsors</h3>
                                            <div class="sponsors-wrap w-100">
                                                <div class="row">
                                                    @foreach ($sponsors as $item)
                                                    <div class="col-md-4 col-sm-4 col-lg-4">
                                                        <div class="spr-box w-100">
                                                            <a href="javascript:void(0);" title=""><img class="img-fluid" src="/images/{{ $item->image }}" alt="Sponsor Image 1"></a>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div class="event-detail-content-inner w-100">
                                            <h3>Faq's</h3>
                                            <div class="toggle w-100" id="toggle">
                                                <div class="toggle-item w-100">
                                                    <h4 class="mb-0">Royal Park of America Produces<i class=""></i></h4>
                                                    <div class="toggle-content w-100"><p class="mb-0">Royal Park of America produces premium branded and private label lubricants for agriculture.</p></div>
                                                </div>
                                                <div class="toggle-item w-100">
                                                    <h4 class="mb-0">Private label Lubricants<i class=""></i></h4>
                                                    <div class="toggle-content w-100"><p class="mb-0">Royal Park of America produces premium branded and private label lubricants for agriculture.</p></div>
                                                </div>
                                                <div class="toggle-item w-100">
                                                    <h4 class="mb-0">Automotive, Fleet<i class=""></i></h4>
                                                    <div class="toggle-content w-100"><p class="mb-0">Royal Park of America produces premium branded and private label lubricants for agriculture.</p></div>
                                                </div>
                                                <div class="toggle-item w-100">
                                                    <h4 class="mb-0">industrial Applications.<i class=""></i></h4>
                                                    <div class="toggle-content w-100"><p class="mb-0">Royal Park of America produces premium branded and private label lubricants for agriculture.</p></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-lg-4">
                                    <div class="sidebar-wrap2 pt-140 w-100">
                                        <div class="widget-box w-100">
                                            <h3 class="thm-bg">Who Host this Event</h3>
                                            <div class="event-organizer w-100">
                                                <div class="tab-content">
                                                    <div class="tab-pane fade show active" id="event-organizer1">
                                                        <div class="event-organizer-box w-100">
                                                            <div class="event-organizer-info d-flex flex-wrap align-items-center w-100">
                                                                @foreach ($host as $item)
                                                                <img class="img-fluid rounded-circle" src="/images/{{ $item->image }}" alt="Event Organizer Image 1">
                                                                <div class="event-organizer-info-inner">
                                                                    <h4 class="mb-0">{{$item->fullname}}</h4>
                                                                    <span class="thm-clr">Posted {{ \Carbon\Carbon::parse($event->created_at)->diffForHumans()}}</span>
                                                                </div>
                                                            </div>
                                                            
                                                                    <ul class="post-meta event-organizer-meta mb-0 list-unstyled w-100">
                                                                        <li><i class="fa fa-envelope rounded-circle"></i>{{$item->email}}</li>
                                                                        <li><i class="fa fa-phone rounded-circle"></i>{{$item->numbers}}</li>
                                                                        <li><i class="fa fa-facebook rounded-circle"></i>{{$item->facebook}}</li>
                                                                        <li><i class="fa fa-twitter rounded-circle"></i>{{$item->twitter}}</li>
                                                                        <li><i class="fa fa-linkedin rounded-circle"></i>{{$item->linkedin}}</li>
                                                                    </ul> 
                                                                    @endforeach
                                                        </div>                    
                                                    </div>
                                                    <div class="tab-pane fade" id="event-organizer2">
                                                        <div class="event-organizer-box w-100">
                                                            <div class="event-organizer-info d-flex flex-wrap align-items-center w-100">
                                                                <img class="img-fluid rounded-circle" src="https://jthemes.net/themes/html/neondir/assets/images/resources/event-organizer-img2.jpg" alt="Event Organizer Image 2">
                                                                <div class="event-organizer-info-inner">
                                                                    <h4 class="mb-0">Mark Joe</h4>
                                                                    <span class="thm-clr">Posted 8 days ago</span>
                                                                </div>
                                                            </div>
                                                            <ul class="post-meta event-organizer-meta mb-0 list-unstyled w-100">
                                                                <li><i class="fas fa-map-marker-alt rounded-circle"></i>484 Ellis St, Francisco, CA 94102.</li>
                                                                <li><i class="far fa-envelope rounded-circle"></i>example@gmail.com</li>
                                                                <li><i class="fas fa-phone rounded-circle"></i>89+97872978129</li>
                                                                <li><i class="fas fa-globe rounded-circle"></i>www.yourwebsite.com</li>
                                                            </ul>
                                                        </div>                    
                                                    </div>
                                                    <div class="tab-pane fade" id="event-organizer3">
                                                        <div class="event-organizer-box w-100">
                                                            <div class="event-organizer-info d-flex flex-wrap align-items-center w-100">
                                                                <img class="img-fluid rounded-circle" src="https://jthemes.net/themes/html/neondir/assets/images/resources/event-organizer-img3.jpg" alt="Event Organizer Image 3">
                                                                <div class="event-organizer-info-inner">
                                                                    <h4 class="mb-0">Willma Mark</h4>
                                                                    <span class="thm-clr">Posted 5 days ago</span>
                                                                </div>
                                                            </div>
                                                            <ul class="post-meta event-organizer-meta mb-0 list-unstyled w-100">
                                                                <li><i class="fas fa-map-marker-alt rounded-circle"></i>484 Ellis St, Francisco, CA 94102.</li>
                                                                <li><i class="far fa-envelope rounded-circle"></i>example@gmail.com</li>
                                                                <li><i class="fas fa-phone rounded-circle"></i>89+97872978129</li>
                                                                <li><i class="fas fa-globe rounded-circle"></i>www.yourwebsite.com</li>
                                                            </ul>
                                                        </div>                    
                                                    </div>
                                                </div>
                                                <ul class="nav nav-tabs">
                                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#event-organizer1"><img class="img-fluid rounded-circle" src="https://jthemes.net/themes/html/neondir/assets/images/resources/event-organizer-nav-img1.jpg" alt="Event Organizer Nav Image 1"></a></li>
                                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#event-organizer2"><img class="img-fluid rounded-circle" src="https://jthemes.net/themes/html/neondir/assets/images/resources/event-organizer-nav-img2.jpg" alt="Event Organizer Nav Image 2"></a></li>
                                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#event-organizer3"><img class="img-fluid rounded-circle" src="https://jthemes.net/themes/html/neondir/assets/images/resources/event-organizer-nav-img3.jpg" alt="Event Organizer Nav Image 3"></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="widget-box w-100">
                                            <h3 class="thm-bg">Event Schedule</h3>
                                            <div class="event-schedule-wrap text-center w-100">
                                                <div class="event-schedule-box w-100">
                                                    <img class="img-fluid rounded-circle" src="https://jthemes.net/themes/html/neondir/assets/images/resources/event-schedule-img1.jpg" alt="Event Schedule Image 1">
                                                    <h4 class="mb-0"><a href="javascript:void(0);" title="">Auditorium A</a></h4>
                                                    <i class="d-block">Introduction to WP</i>
                                                    <span>Day 1 - 20 Nov 2020</span>
                                                </div>
                                                <div class="event-schedule-box w-100">
                                                    <img class="img-fluid rounded-circle" src="https://jthemes.net/themes/html/neondir/assets/images/resources/event-schedule-img2.jpg" alt="Event Schedule Image 2">
                                                    <h4 class="mb-0"><a href="javascript:void(0);" title="">Auditorium A</a></h4>
                                                    <i class="d-block">Introduction to WP</i>
                                                    <span>Day 2 - 21 Nov 2020</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- Event Detail Wrap 2 -->
                    </div>
                </div>
            </section>
            <footer>
                <div class="w-100 dark-bg2 position-relative">
                    <div class="footer-cont-wrap w-100">
                        <div class="container">
                            <div class="footer-cont-inner d-flex flex-wrap align-items-center justify-content-between">
                                <div class="footer-cont-info">
                                    <ul class="mb-0 list-unstyled d-inline-flex">
                                        <li><i class="rounded thm-bg fas fa-phone"></i>Free support: +01 5426 24400</li>
                                        <li><i class="rounded thm-bg fas fa-envelope"></i>Email: <a href="javascript:void(0);" title="">Example@example.com</a></li>
                                    </ul>
                                </div>
                                <div class="footer-cont-btn">
                                    <a class="thm-btn rounded" href="about.html" title="">About Us</a>
                                </div>
                            </div>
                        </div>
                    </div><!-- Footer Contact Wrap -->
                    <div class="footer-data w-100">
                        <div class="container res-row">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-lg-3">
                                    <div class="widget w-100">
                                        <div class="logo"><h1 class="mb-0"><a href="index.html" title="Home"><img class="img-fluid" src="assets/images/logo.png" alt="Logo" srcset="assets/images/retina-logo.png"></a></h1></div>
                                        <p class="mb-0">Lorem ipsum dolor sit amet, conse ctetuers adipiscing eli sed diam nonum nibhieLorem ipsum dolor sit amet, conse ctetuers adipiscing eli sed diam nonum nibhie……</p>
                                        <div class="social-links d-inline-block">
                                            <a href="javascript:void(0);" title="Facebook" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                            <a href="javascript:void(0);" title="Twitter" target="_blank"><i class="fab fa-twitter"></i></a>
                                            <a href="javascript:void(0);" title="Google Plus" target="_blank"><i class="fab fa-google-plus-g"></i></a>
                                            <a href="javascript:void(0);" title="Linkedin" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-lg-5">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-lg-6">
                                            <div class="widget w-100">
                                                <h3>Userful Links</h3>
                                                <ul class="mb-0 list-unstyled w-100">
                                                    <li><a href="javascript:void(0);" title="">About</a></li>
                                                    <li><a href="javascript:void(0);" title="">Our Story</a></li>
                                                    <li><a href="javascript:void(0);" title="">Professional Team</a></li>
                                                    <li><a href="javascript:void(0);" title="">Services</a></li>
                                                    <li><a href="javascript:void(0);" title="">Testimonials</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-lg-6">
                                            <div class="widget w-100">
                                                <h3>Trending Category</h3>
                                                <ul class="mb-0 list-unstyled w-100">
                                                    <li><a href="javascript:void(0);" title="">Automobiles</a></li>
                                                    <li><a href="javascript:void(0);" title="">Club party Night</a></li>
                                                    <li><a href="javascript:void(0);" title="">Car showroom</a></li>
                                                    <li><a href="javascript:void(0);" title="">School Event</a></li>
                                                    <li><a href="javascript:void(0);" title="">Gym event</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-lg-4">
                                    <div class="widget w-100">
                                        <h3>Gallery</h3>
                                        <ul class="gallery-list mb-0 list-unstyled d-flex flex-wrap">
                                            <li><a href="javascript:void(0);" title=""><img class="img-fluid w-100" src="https://jthemes.net/themes/html/neondir/assets/images/resources/gal-img1-1.jpg" alt="Gallery Image 1"></a></li>
                                            <li><a href="javascript:void(0);" title=""><img class="img-fluid w-100" src="https://jthemes.net/themes/html/neondir/assets/images/resources/gal-img1-2.jpg" alt="Gallery Image 2"></a></li>
                                            <li><a href="javascript:void(0);" title=""><img class="img-fluid w-100" src="https://jthemes.net/themes/html/neondir/assets/images/resources/gal-img1-3.jpg" alt="Gallery Image 3"></a></li>
                                            <li><a href="javascript:void(0);" title=""><img class="img-fluid w-100" src="https://jthemes.net/themes/html/neondir/assets/images/resources/gal-img1-4.jpg" alt="Gallery Image 4"></a></li>
                                            <li><a href="javascript:void(0);" title=""><img class="img-fluid w-100" src="https://jthemes.net/themes/html/neondir/assets/images/resources/gal-img1-5.jpg" alt="Gallery Image 5"></a></li>
                                            <li><a href="javascript:void(0);" title=""><img class="img-fluid w-100" src="https://jthemes.net/themes/html/neondir/assets/images/resources/gal-img1-6.jpg" alt="Gallery Image 6"></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- Footer Data -->
                </div>
            </footer><!-- Footer -->
            <div class="copyright text-center w-100">
                <div class="container">
                    <p class="mb-0">&copy; 2020 <a href="index.html" title="">NeonDir</a> — Directory & Listings WordPress Theme. Developed by <a href="https://themeforest.net/user/jthemes/portfolio" title="Jthemes" target="_blank">Jthemes</a></p>
                </div>
            </div><!-- Copyright -->
        </main><!-- Main Wrapper -->

        <script src="https://jthemes.net/themes/html/neondir/assets/js/jquery.min.js"></script>
        <script src="https://jthemes.net/themes/html/neondir/assets/js/popper.min.js"></script>
        <script src="https://jthemes.net/themes/html/neondir/assets/js/bootstrap.min.js"></script>
        <script src="https://jthemes.net/themes/html/neondir/assets/js/bootstrap-select.min.js"></script>
        <script src="https://jthemes.net/themes/html/neondir/assets/js/wow.min.js"></script>
        <script src="https://jthemes.net/themes/html/neondir/assets/js/counterup.min.js"></script>
        <script src="https://jthemes.net/themes/html/neondir/assets/js/scroll-up-bar.min.js"></script>
        <script src="https://jthemes.net/themes/html/neondir/assets/js/jquery.fancybox.min.js"></script>
        <script src="https://jthemes.net/themes/html/neondir/assets/js/perfect-scrollbar.min.js"></script>
        <script src="https://jthemes.net/themes/html/neondir/assets/js/styleswitcher.js"></script>
        <script src="https://jthemes.net/themes/html/neondir/assets/js/slick.min.js"></script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYc537bQom7ajFpWE5sQaVyz1SQa9_tuY&callback=initMap"></script>
        <script src="https://jthemes.net/themes/html/neondir/assets/js/google-map-int.js"></script>
        <script src="https://jthemes.net/themes/html/neondir/assets/js/custom-scripts.js"></script>
    </body>	
</html>