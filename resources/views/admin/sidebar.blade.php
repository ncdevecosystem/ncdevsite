<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
</head>
<body>
    <nav class="mt-10">
        <a class="flex items-center mt-4 py-2 px-6 text-gray-500 hover:bg-gray-700 hover:bg-opacity-25 hover:text-gray-100 text-gray-100" href="/home"><i class="fa fa-user"></i>
        <span class="mx-3"> Dashboard</span></a>
        
        <a class="flex items-center mt-4 py-2 px-6 text-gray-500 hover:bg-gray-700 hover:bg-opacity-25 hover:text-gray-100" href="/blog_admin">
        <i class="fa fa-tags"></i><span class="mx-3"> Blog Editor</span></a>
        
        <a class="flex items-center mt-4 py-2 px-6 text-gray-500 hover:bg-gray-700 hover:bg-opacity-25 hover:text-gray-100"
        href="/admin/events/event_list"> <i class="fa fa-calendar"></i>
        <span class="mx-3"> Events Manager</span>
        </a>
        
        <a class="flex items-center mt-4 py-2 px-6 text-gray-500 hover:bg-gray-700 hover:bg-opacity-25 hover:text-gray-100"
        href="/laravel-filemanager#"> <i class="fa fa-folder"></i>
        <span class="mx-3"> File Manager</span>
        </a>
        
        <a class="flex items-center mt-4 py-2 px-6 text-gray-500 hover:bg-gray-700 hover:bg-opacity-25 hover:text-gray-100"
        href="/dashboard/media"> <i class="fa fa-group"></i>
        <span class="mx-3"> Community</span>
        </a>
        
        <a class="flex items-center mt-4 py-2 px-6 text-gray-500 hover:bg-gray-700 hover:bg-opacity-25 hover:text-gray-100"
        href="/dashboard/short-link"><i class="fa fa-link"></i>
        <span class="mx-3">VacWork</span>
        </a>
        <a class="flex items-center mt-4 py-2 px-6 text-gray-500 hover:bg-gray-700 hover:bg-opacity-25 hover:text-gray-100"
        href="/dashboard/assets"> <i class="fa fa-cogs"></i>
        <span class="mx-3">Settings</span>
        </a>
        </nav>
</body>
</html>