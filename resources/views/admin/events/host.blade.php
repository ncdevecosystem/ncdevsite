@extends('admin.layout')

@section('content')

<link rel="stylesheet" href="https://unpkg.com/tailwindcss@2.1.4/dist/tailwind.min.css">
<!-- component -->
<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        @if (session('status'))
                <h6 class="alert alert-success">{{ session('status') }}</h6>
            @endif
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
                <form enctype="multipart/form-data" method="POST" action="/admin/events/host">
                    @csrf
                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Event <span class="text-red-500">*</span></label></br>
                        <select name="event" class="border-2 border-gray-300 p-2 w-full" required>
                            @foreach ($event as $item)
                             <option value="{{$item->id}}">{{$item->event}}</option>
                            @endforeach
                           
                        </select>
                    </div>
                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Fullname <span class="text-red-500">*</span></label></br>
                        <input type="text" class="border-2 border-gray-300 p-2 w-full" name="fullname"  placeholder="fullname">
                    </div>
                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Email <span class="text-red-500">*</span></label></br>
                        <input type="email" class="border-2 border-gray-300 p-2 w-full" name="email"  placeholder="email@gmail.com">
                    </div>
                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Phone Number <span class="text-red-500">*</span></label></br>
                        <input type="tel" class="border-2 border-gray-300 p-2 w-full" name="numbers"  placeholder="Phone Number">
                    </div>
                    <div>
                        <label class="text-xl text-gray-600">Host's Image (240 X 240) <span class="text-red-500">*</span></label></br>
                        <input type="file" class="border-2 border-gray-300 p-2 w-full" name="image"  accept="image/*">
                    </div>

                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Facebook <span class="text-red-500">*</span></label></br>
                        <input type="text" class="border-2 border-gray-300 p-2 w-full" name="facebook"  placeholder="facebook">
                    </div>
                    <div class="mb-4">
                        <label class="text-xl text-gray-600">LinkedIn <span class="text-red-500">*</span></label></br>
                        <input type="text" class="border-2 border-gray-300 p-2 w-full" name="linkedin"  placeholder="linkedin">
                    </div>
                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Twitter <span class="text-red-500">*</span></label></br>
                        <input type="text" class="border-2 border-gray-300 p-2 w-full" name="twitter"  placeholder="twitter">
                    </div>
                    <div class="flex p-1">
                        <button role="submit" class="p-3 bg-blue-500 text-white hover:bg-blue-400" required>Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script>
    CKEDITOR.replace( 'description' );
</script>

@endsection