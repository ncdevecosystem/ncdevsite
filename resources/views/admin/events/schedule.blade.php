@extends('admin.layout')

@section('content')

<link rel="stylesheet" href="https://unpkg.com/tailwindcss@2.1.4/dist/tailwind.min.css">
<!-- component -->
<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        @if (session('status'))
                <h6 class="alert alert-success">{{ session('status') }}</h6>
            @endif
            <div>
                <a href="/admin/events/sponsors" class="bg-red-600 hover:bg-gray-900 cursor-pointer text-white font-bold py-2 ml-6 px-4 rounded"><i class="fa fa-plus"></i> Add Partners</a>
            <a href="/admin/events/host" class="bg-yellow-600 hover:bg-gray-900 cursor-pointer text-white font-bold py-2 ml-6 px-4 rounded"><i class="fa fa-group"></i> Add Host's Info</a>
            <a href="/admin/events/speakers" class="bg-green-600 hover:bg-gray-900 cursor-pointer text-white font-bold py-2 ml-6 px-4 rounded"><i class="fa fa-mic"></i> Add Speakers</a>
            <a href="/admin/events/audience_report" class="bg-green-600 hover:bg-gray-900 cursor-pointer text-white font-bold py-2 ml-6 px-4 rounded"><i class="fa fa-mic"></i> Add Event Schedule</a>
            </div>
            
            <br>   
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
                <form enctype="multipart/form-data" method="POST" action="/admin/events/schedule">
                    @csrf
                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Event <span class="text-red-500">*</span></label></br>
                        <select name="event" class="border-2 border-gray-300 p-2 w-full" required>
                            @foreach ($event as $item)
                             <option value="{{$item->id}}">{{$item->event}}</option>
                            @endforeach
                           
                        </select>
                    </div>
                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Date <span class="text-red-500">*</span></label></br>
                        <input type="text" class="border-2 border-gray-300 p-2 w-full" name="date" id="title" value="" required>
                    </div>
                    <div class="mb-8">
                        <label class="text-xl text-gray-600">Description <span class="text-red-500">*</span></label></br>
                        <textarea name="description" id="description" class="border-2 border-gray-500">
                            
                        </textarea>
                    </div>
                  
                    <div class="flex p-1">
                        <button role="submit" class="p-3 bg-blue-500 text-white hover:bg-blue-400" required>Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

<script>
    CKEDITOR.replace( 'description' );
</script>

@endsection