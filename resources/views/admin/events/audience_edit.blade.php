@extends('admin.layout')

@section('content')

<link rel="stylesheet" href="https://unpkg.com/tailwindcss@2.1.4/dist/tailwind.min.css">

<!-- themefy CSS -->

<!-- component -->
<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        @if (session('status'))
                <h6 class="alert alert-success">{{ session('status') }}</h6>
            @endif
            <br>
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
                <form enctype="multipart/form-data" method="POST" action="/admin/events/audience_edit">
                    @csrf
                    <input type="hidden" class="border-2 border-gray-300 p-2 w-full" name="id" id="title" value="{{$vacwork->id}}" required>
                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Name <span class="text-red-500">*</span></label></br>
                        <input type="text" class="border-2 border-gray-300 p-2 w-full" name="event" id="title" value="{{$vacwork->first_name}}" required>
                    </div>

                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Email <span class="text-red-500">*</span></label></br>
                        <input type="text" class="border-2 border-gray-300 p-2 w-full" name="event" id="title" value="{{$vacwork->email}}" required>
                    </div>

                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Contact<span class="text-red-500">*</span></label></br>
                        <input type="text" class="border-2 border-gray-300 p-2 w-full" name="starting" value="{{$vacwork->contact}}">
                    </div>

                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Address <span class="text-red-500">*</span></label></br>
                        <input type="text" class="border-2 border-gray-300 p-2 w-full" name="ending"  value="{{$vacwork->address}}">
                    </div>

                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Status <span class="text-red-500">*</span></label></br>
                        <select name="status" class="border-2 border-gray-300 p-2 w-full">
                            <option value="0">For Verification</option>
                            <option value="1">confirmed</option>
                        </select>
                    </div>

                  
                    <div class="flex p-1">
                        <button role="submit" class="p-3 bg-blue-500 text-white hover:bg-blue-400" required>Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

<script>
    CKEDITOR.replace( 'description' );
</script>
@endsection