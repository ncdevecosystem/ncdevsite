@extends('admin.layout')

@section('content')

<link rel="stylesheet" href="https://unpkg.com/tailwindcss@2.1.4/dist/tailwind.min.css">

<!-- themefy CSS -->

<!-- component -->
<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
                <form enctype="multipart/form-data" method="POST" action="/admin/events/edit">
                    @csrf
                    
                    <input type="hidden" class="border-2 border-gray-300 p-2 w-full" name="id" id="title" value="{{$vacwork->id}}" required>
                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Event <span class="text-red-500">*</span></label></br>
                        <input type="text" class="border-2 border-gray-300 p-2 w-full" name="event" id="title" value="{{$vacwork->event}}" required>
                    </div>

                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Host <span class="text-red-500">*</span></label></br>
                        <input type="text" class="border-2 border-gray-300 p-2 w-full" name="host" id="title" value="{{$vacwork->host}}" required>
                    </div>

                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Schedule - Start <span class="text-red-500">*</span></label></br>
                        <input type="text" class="border-2 border-gray-300 p-2 w-full" name="starting" value="{{$vacwork->starting}}">
                    </div>

                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Schedule - End <span class="text-red-500">*</span></label></br>
                        <input type="text" class="border-2 border-gray-300 p-2 w-full" name="ending"  value="{{$vacwork->ending}}">
                    </div>

                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Venue <span class="text-red-500">*</span></label></br>
                        <input type="text" class="border-2 border-gray-300 p-2 w-full" name="venue"  value="{{$vacwork->venue}}">
                    </div>

                    <div class="mb-8">
                        <label class="text-xl text-gray-600">Description <span class="text-red-500">*</span></label></br>
                        <textarea name="description" id="description" class="border-2 border-gray-500" >
                            {{$vacwork->description}}
                        </textarea>
                    </div>
                    <div class="mb-4">
                        <label class="text-xl text-gray-600">cost <span class="text-red-500">*</span></label></br>
                        <input type="text" class="border-2 border-gray-300 p-2 w-full" name="cost"  value="{{$vacwork->cost}}">
                    </div>
                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Audience Capacity <span class="text-red-500">*</span></label></br>
                        <input type="number" class="border-2 border-gray-300 p-2 w-full" name="audience"  value="{{$vacwork->audience}}">
                    </div>
                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Banner Image <span class="text-red-500">*</span></label></br>
                        <input type="text" class="border-2 border-gray-300 p-2 w-full" name="image"  value="{{$vacwork->image}}">
                    </div>
                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Link to the event <span class="text-red-500">*</span></label></br>
                        <input type="text" class="border-2 border-gray-300 p-2 w-full" name="url"  value="{{$vacwork->url}}">
                    </div>
                    <div class="flex p-1">
                        <button role="submit" class="p-3 bg-blue-500 text-white hover:bg-blue-400" required>Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

<script>
    CKEDITOR.replace( 'description' );
</script>
@endsection