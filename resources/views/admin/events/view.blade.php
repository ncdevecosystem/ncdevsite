@extends('admin.layout')

@section('content')
<style >
	.imgs{
		margin: .5em;
		max-width: calc(100%);
		max-height: calc(100%);
	}
	.imgs img{
		max-width: calc(100%);
		max-height: calc(100%);
		cursor: pointer;
	}
	#imagesCarousel,#imagesCarousel .carousel-inner,#imagesCarousel .carousel-item{
		height: 40vh !important;background: black;

	}
	#imagesCarousel{
		margin-left:unset !important ;
	}
	#imagesCarousel .carousel-item.active{
		display: flex !important;
	}
	#imagesCarousel .carousel-item-next{
		display: flex !important;
	}
	#imagesCarousel .carousel-item img{
		margin: auto;
		margin-top: unset;
		margin-bottom: unset;
	}
	#imagesCarousel img{
		width: calc(100%)!important;
		height: auto!important;
		/*max-height: calc(100%)!important;*/
		max-width: calc(100%)!important;
		cursor :pointer;
	}
	#banner{
		display: flex;
		justify-content: center;
	}
	#banner img{
		max-width: calc(100%);
		max-height: 50vh;
		cursor :pointer;
	}
</style>
<div class="container-field">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<div id="banner" class='mx-2'>
								<img src="/uploads/avatars/{{ $vacwork->image }}" alt="">
						</div>
					<br>
					</div>
					<div class="col-md-12">
						<h4 class="text-center"><b>{{ $vacwork->event}}</b></h4>
						<p class="text-center"><small><b><i>Venue : {{ $vacwork->venue}}</small></i></b></p>
						<hr class="divider" style="max-width:calc(100%)">
					</div>
					<div class="col-md-12" id="content">
						
						<div id="imagesCarousel" class="carousel slide col-sm-4 float-left  ml-0 mx-4"  data-ride="carousel">

							  <div class="carousel-inner">
								
						
					  	 <a class="carousel-control-prev" href="#imagesCarousel" role="button" data-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#imagesCarousel" role="button" data-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a>
					  		</div>
					  		
						</div>
					<p class="">
						
						<p><b><i class="fa fa-calendar"></i> {{ \Carbon\Carbon::parse($vacwork->starting)->format(' H.i  A  d-m-y ') }}</b>&nbsp;&nbsp;&nbsp;&nbsp;<b><i class="fa fa-calendar"></i> {{ \Carbon\Carbon::parse($vacwork->ending)->format(' H.i  A  d-m-y ') }}</b></p>
						{!!$vacwork->description !!}
					</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$('#imagesCarousel img,#banner img').click(function(){
		viewer_modal($(this).attr('src'))
	})
	$('.carousel').carousel()
</script>

@endsection
