@extends('admin.layout')

@section('content')

<div class="container-fluid">
<style>
	input[type=checkbox]
{
  /* Double-sized Checkboxes */
  -ms-transform: scale(1.5); /* IE */
  -moz-transform: scale(1.5); /* FF */
  -webkit-transform: scale(1.5); /* Safari and Chrome */
  -o-transform: scale(1.5); /* Opera */
  transform: scale(1.5);
  padding: 10px;
}
</style>
	<div class="col-lg-12">
		<div class="row mb-4 mt-4">
			<div class="col-md-12">
				
			</div>
		</div>
		<div class="row">
			<!-- FORM Panel -->

			<!-- Table Panel -->
			<div class="col-md-12">
				@if (session('status'))
                <h6 class="alert alert-danger">{{ session('status') }}</h6>
               @endif
				<div class="card">
					<div class="card-header">
						<b>Event Audience List</b>
						<span class="">

							<button class="btn btn-danger btn-block btn-sm col-sm-2 float-right" type="button" id="new_register">
					<i class="fa fa-plus"></i> New</button>
				</span>
					</div>
					<div class="card-body">
						
						<table class="table table-bordered table-condensed table-hover">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="">Event Information</th>
									<th class="">Audience Information</th>
									<th class="">Status</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($audience as $item)
								<tr>
									
									<td class="text-center"{{$item->id}}></td>
									<td class="">
										 <p>Event: <b>{{$item->event}}</b></p>
										 <p><small>Schedule start: <b>{{ \Carbon\Carbon::parse($item->starting)->format('D M Y  H.i  A ') }}</b></small></p>
										 <p><small>Schedule End: <b>{{ \Carbon\Carbon::parse($item->ending)->format('D M Y  H.i  A ') }}</b></small></p>
										 
									</td>
									<td class="">
										 <p>Name: <b>{{$item->first_name}} {{$item->last_name}}</b></p>
										 <p><small>Gender: <b>{{$item->gender}}</b></small></p>
										 <p><small>Email: <b>{{$item->email}}</b></small></p>
										 <p><small>Organisation / Institution: <b>{{$item->org}}</b></small></p>
										 <p><small>Background: <b>{{$item->background}}</b></small></p>
										 <p><small>Contact: <b>{{$item->contact}}</b></small></p>
										 <p><small>Address: <b>{{$item->address}}</b></small></p>
										
									</td>
									<td class="text-center">
										 
										 	<span class="px-2 py-1 font-semibold leading-tight text-700 {{$item->status=='1' ? 'badge badge-primary' : 'badge badge-secondary'}}  "> {{ $item->status=='1' ? 'Confirmed' : 'For Verification' }} </span>
										 
									</td>
									<td class="text-center">
									
										@if($item->status=='1')
										<a href="/send-mail/{{ $item->id }}" class="btn btn-sm btn-outline-primary">Send Email</a>
									  @else
									  @endif

									  @if($item->status=='1')
										<a href="#" class="btn btn-sm btn-outline-primary">Send Sms</a>
									  @else
									  @endif
										
										<a href="/admin/events/audience_edit/{{$item->id}}" class="btn btn-sm btn-outline-primary edit_register" type="button" data-id="" >Edit</a>
										
										<a href="/admin/events/audience/{{$item->id}}" class="btn btn-sm btn-outline-danger delete_register" type="button" data-id="">Delete</a>
										
									</td>
								</tr>
								
								@endforeach
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- Table Panel -->
		</div>
	</div>	

</div>
<style>
	
	td{
		vertical-align: middle !important;
	}
	td p{
		margin: unset
	}
	img{
		max-width:100px;
		max-height: :150px;
	}
</style>


@endsection
