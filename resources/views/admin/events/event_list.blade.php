@extends('admin.layout')

@section('content')

<section class="mx-auto p-6">
	@if (session('status'))
        <h6 class="alert alert-danger">{{ session('status') }}</h6>
     @endif
	<div class="w-full mb-8 overflow-hidden rounded-lg shadow-lg">
	<div class="w-full overflow-x-auto">
	<div class="bg-white py-4 px-4">
	<input type="text" id="myInput" onkeyup="myFunction()" class="form border px-2 py-2" placeholder="Search Events">
	<a href="/admin/events/create" class="bg-red-600 hover:bg-gray-900 cursor-pointer text-white font-bold py-2 ml-6 px-4 rounded"><i class="fa fa-plus"></i> Add Event</a>
	<a href="/admin/events/audience" class="bg-yellow-600 hover:bg-gray-900 cursor-pointer text-white font-bold py-2 ml-6 px-4 rounded"><i class="fa fa-group"></i>  Events Audience List</a>
	<a href="/admin/events/audience_report" class="bg-green-600 hover:bg-gray-900 cursor-pointer text-white font-bold py-2 ml-6 px-4 rounded"><i class="fa fa-download"></i> Report</a>
	</div>
	<table id="myTable" class="w-full" style="overflow-x: hidden;">
	<thead>
	<tr class="text-md tracking-wide text-left text-gray-900 bg-gray-100 border-b border-gray-600">
	<th class="px-4 py-3">ID</th>
	<th class="px-4 py-3">Name</th>
	<th class="px-4 py-3">Host</th>
	<th class="px-4 py-3">Cost</th>
	<th class="px-4 py-3">Location</th>
	<th class="px-4 py-3">Category</th>
	<th class="px-4 py-3">Description</th>
	<th class="px-4 py-3">Action</th>
	</tr>
	</thead>
	<tbody class="bg-white">
	@foreach ($vacwork as $vacwork)
	<tr class="text-gray-700">
	<td class="px-4 py-3 text-ms border">{{$vacwork->id}}</td>
	<td class="px-4 py-3 border">
	<div class="flex items-center text-sm">
	<div>
	<a href="{{$vacwork->id}}" target="_blank" class="text-black">{{$vacwork->event}}</a>
	</div>
	</div>
	</td>
	<td class="px-4 py-3 text-ms border">{{$vacwork->host}}</td>
	<td class="px-4 py-3 text-xs border">
	<span class="px-2 py-1  leading-tight text-green-700 bg-green-100 rounded-sm ">{{$vacwork->cost}} </span>
	</td>
	<td class="px-4 py-3 border">{{$vacwork->venue}}</td>
	<td class="px-4 py-3 text-xs border">{{$vacwork->categories}} </td>
	<td class="px-4 py-3 text-sm border"><p>{!! Str::limit($vacwork->description, 50) !!} <a href="/admin/events/view/{{$vacwork->id}}"> <span class="px-2 py-1 font-semibold leading-tight text-sm rounded-sm">...read more</span></a></p></td>
	<td class="px-4 py-3 text-ms border"><!-- component -->
	<a href="/admin/events/edit/{{$vacwork->id}}" class="bg-transparent text-gray-500 hover:text-green-500 py-2 rounded-full"><i class="fa fa-edit"></i></a>
	<a href="/admin/events/event_list/{{$vacwork->id}}" onclick="return confirm('Are you sure you want to delete the event, {{$vacwork->event}}?')" class="bg-transparent text-gray-500 hover:text-red-500 py-2 rounded-full"><i class="fa fa-trash"></i> </a>
	<dialog id="myModal6" class="h-auto w-11/12 md:w-1/2 p-5 bg-white rounded-md ">
	<div class="flex flex-col w-full h-auto ">
	</div>
	</div></dialog></td>
	</tr>
	@endforeach
	</tbody>
	</table>
	</div>
	</div>
	</section>

	<script>
		function myFunction() {
		// Declare variables
		var input, filter, table, tr, td, i, txtValue;
		input = document.getElementById("myInput");
		filter = input.value.toUpperCase();
		table = document.getElementById("myTable");
		tr = table.getElementsByTagName("tr");
		// Loop through all table rows, and hide those who don't match the search query
		for (i = 0; i < tr.length; i++) {
		td = tr[i].getElementsByTagName("td")[1];
		if (td) {
		txtValue = td.textContent || td.innerText;
		if (txtValue.toUpperCase().indexOf(filter) > -1) {
		tr[i].style.display = "";
		} else {
		tr[i].style.display = "none";
		}
		}
		}
		}
		</script>

@endsection



