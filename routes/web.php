<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AdminController;

use App\Http\Controllers\HomeController;

use App\Http\Controllers\SponsorsController;

use App\Http\Controllers\HostController;

use App\Http\Controllers\SpeakersController;

use App\Http\Controllers\ScheduleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->middleware('auth', 'admin');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'charts'])->middleware('auth', 'admin');

//event

Route::get('/admin/events/create', [AdminController::class, 'event_create'])->middleware('auth', 'admin');

Route::post('/admin/events/create', [AdminController::class, 'event_store'])->middleware('auth', 'admin');

Route::get('/admin/events/event_list', [AdminController::class, 'event_show'])->middleware('auth', 'admin');

Route::get('/admin/events/event_list/{id}', [AdminController::class, 'event_delete'])->middleware('auth', 'admin');

Route::get('/admin/events/audience', [AdminController::class, 'audience'])->middleware('auth', 'admin');

Route::get('/admin/events/audience', [AdminController::class, 'audience_view'])->middleware('auth', 'admin');

Route::get('/admin/events/edit/{id}', [AdminController::class, 'event'])->middleware('auth', 'admin');

Route::post('/admin/events/edit', [AdminController::class, 'event_edit'])->middleware('auth', 'admin');

Route::get('/admin/events/edit/{id}', [AdminController::class, 'event_edit_view'])->middleware('auth', 'admin');

Route::get('/admin/events/view/{id}', [AdminController::class, 'event_view'])->middleware('auth', 'admin');

Route::get('/admin/events/audience_report', [AdminController::class, 'audience_report'])->middleware('auth', 'admin');

Route::get('/admin/events/audience_edit/{id}', [AdminController::class, 'view_register'])->middleware('auth', 'admin');

Route::post('/admin/events/audience_edit', [AdminController::class, 'update_register'])->middleware('auth', 'admin');

Route::get('/admin/events/audience/{id}', [AdminController::class, 'delete_register'])->middleware('auth', 'admin');

Route::get('/admin/events/view1', [AdminController::class, 'download'])->middleware('auth', 'admin');

Route::get('/admin/events/view2', [AdminController::class, 'export'])->middleware('auth', 'admin');

Route::post('/send-mail/{id}', [AdminController::class, 'sendemail'])->middleware('auth', 'admin');

Route::get('/send-mail/{id}', [AdminController::class, 'sendemail'])->middleware('auth', 'admin');

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

//sponsors
Route::get('/admin/events/sponsors', [SponsorsController::class, 'index'])->middleware('auth', 'admin');

Route::get('/admin/events/sponsors', [SponsorsController::class, 'view'])->middleware('auth', 'admin');

Route::post('/admin/events/sponsors', [SponsorsController::class, 'store'])->middleware('auth', 'admin');

Route::get('/admin/events/sponsors/{id}', [SponsorsController::class, ' destroy'])->middleware('auth', 'admin');

//Host
Route::get('/admin/events/host', [HostController::class, 'index'])->middleware('auth', 'admin');

Route::get('/admin/events/host', [HostController::class, 'view'])->middleware('auth', 'admin');

Route::post('/admin/events/host', [HostController::class, 'store'])->middleware('auth', 'admin');

//Schedule
Route::get('/admin/events/schedule', [ScheduleController::class, 'index'])->middleware('auth', 'admin');

Route::get('/admin/events/schedule', [ScheduleController::class, 'view'])->middleware('auth', 'admin');

Route::post('/admin/events/schedule', [ScheduleController::class, 'store'])->middleware('auth', 'admin');


//Speakers

Route::get('/admin/events/speakers', [SpeakersController::class, 'index'])->middleware('auth', 'admin');

Route::get('/admin/events/speakers', [SpeakersController::class, 'view'])->middleware('auth', 'admin');

Route::post('/admin/events/speakers', [SpeakersController::class, 'store'])->middleware('auth', 'admin');



Route::get('/booking', [HomeController::class, 'booking_view']);

Route::post('/booking', [HomeController::class, 'booking_create']);

Route::get('/booking/{id}', [HomeController::class, 'booking']);

Route::get('/event', [HomeController::class, 'event_view']);

Route::get('/event_details/{id}', [HomeController::class, 'event_details']);

Route::get('/booking_msg', [HomeController::class, 'booking_msg']);

Route::get('pagenotfound', ['as' => 'notfound', 'uses' => 'HomeController@pagenotfound']);


Route::get('/about', function () {
    return view('about');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/initiative', function () {
    return view('initiative');
});

Route::get('/community', function () {
    return view('community');
});

Route::get('/partners', function () {
    return view('partners');
});



