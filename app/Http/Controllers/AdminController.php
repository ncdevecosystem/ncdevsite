<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use PDF;

use Excel;

use App\Models\Event;

use App\Models\Booking;


class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function event_create(){
    
        return view('admin.events.create');
        
      }
        public function event_store(Request $request)
        {
            $request->validate([
                'event' => 'required',
                'starting' => 'required',
                'host' => 'required',
                'cost' => 'required',
                'categories' => 'required',
                'ending' => 'required',
                'venue' => 'required',
                'description' => 'required',
                'audience' => 'required',
                'url' => 'required',
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
      
            $input = $request->all();
      
            if ($image = $request->file('image')) {
                $destinationPath = '/uploads/avatars/';
                $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
                $image->move(public_path('/uploads/avatars/'), $profileImage);
                $input['image'] = "$profileImage";
            }
        
            Event::create($input);
         
            return redirect()->back()->with('status','Event was successfully added');
        }
    
        public function event_show(){
          $vacwork = Event::all();
    
          return view('/admin/events/event_list', compact('vacwork'));
        }
    
        public function event_delete($id){
          DB::table('events')->where('id', '=', $id)->delete(); 
          return redirect()->back()->with('status','Audience successfully deleted');
        }
    
        public function event_view($id){
          $vacwork =  Event::find($id);
          return view('/admin/events/view' , compact('vacwork'));
        }
    
    
        public function event_edit_view($id){
          $vacwork =  Event::find($id);
          return view('/admin/events/edit', compact('vacwork'));
        }
    
        public function event_edit(Request $request)
        {
          $id =$request->input('id');
          $event =$request->input('event');
          $host =$request->input('host');
          $starting =$request->input('starting');
          $ending =$request->input('ending');
          $venue =$request->input('venue');
          $cost =$request->input('cost');
          $slug =$request->input('slug');
          $audience =$request->input('audience');
          $image =$request->input('image');
          $url =$request->input('url');

        DB::update('update events set event_id ="1", event ="'.$event.'", host ="'.$host.'", starting ="'.$starting.'", ending ="'.$ending.'", venue ="'.$venue.'", cost ="'.$cost.'", audience ="'.$audience.'", image ="'.$image.'", url ="'.$url.'"  where id="'.$id.'"');
        return redirect()->back()->with('status','Category successfully updated');
      
        }

        public function event($id)
        {
          $vacwork =  Event::find($id);
          return view('/admin/events/edit', compact('vacwork'));
      
        }

        public function audience(){
          return view('/admin/events/audience');
        }

        public function audience_view(){
          $audience = DB::table('events')
      ->join('bookings', 'events.id', '=', 'bookings.event_id')
      ->get(); 
          return view('/admin/events/audience', compact('audience'));
         
        }

        public function audience_report(){
          if (request()->event ) {
            $event ->event = request()->event;
            $audience = Booking::whereBetween('created_at',[$event])->get();
        } else {
            $audience = Booking::latest()->get();
        }
        
        $event = DB::table('events')
        ->join('bookings', 'events.categories', '=', 'bookings.id')
        ->get();

        return view('/admin/events/audience_report', compact('audience', 'event'));
          
        }

        public function delete_register($id){
          DB::delete('delete from bookings where id = '.$id);
    
          return redirect()->back()->with('status','Audience successfully deleted');
        }

        public function view_register($id){
          $vacwork = Booking::find($id);
        return view('/admin/events/audience_edit' , compact('vacwork'));
       
        }

        public function update_register(Request $request ){
       $id=$request->input('id');
      $status=$request->input('status');
    

    DB::update('update bookings set status="'.$status.'" where id="'.$id.'"');
    return redirect()->back()->with('status',' Audience successfully updated');
       
        }

      public function download()
      {
        $booking = Booking::all();
        $pdf=PDF::loadView('admin.events.view1',compact('booking'));
        return $pdf->download('report_audience.pdf');
    
    }

    public function export()
    {
      return Excel::download( Booking::all(), 'users.xlsx');
    }
  
    public function sendemail($id)
      {
        
         $user = Booking::find($id);
         
         $details = [
        'title' => 'Mail from ItSolutionStuff.com',
        'body' => 'This is for testing email using smtp'
        ];
   
       \Mail::to($user->email)->send(new \App\Mail\MyTestMail($details));
   
        dd("Email is Sent.");
      }  
        
}
