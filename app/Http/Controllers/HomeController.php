<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\Models\Event;

use App\Models\User;

use App\Models\Booking;

use App\Models\Host;

use App\Models\Speakers;

use Carbon\Carbon;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       $event = Event::all();
       $booking = Booking::all();
       $post = DB::table('blog_etc_posts')->select('id')->get();
        return view('home', compact('event', 'post', 'booking'));
    }

    public function booking_view(){
        return view('booking');
      }

      public function booking_create(Request $request,){
        Booking::create($request->except('_token'));

      $event = Event::all();
   
      return view('/booking_msg');
      }  

      public function booking($id){
        $event = Event::find($id);
        return view('/booking', compact('event'));
      } 

      public function event_view(){
      $event = Event::all();
      $shareComponent = \Share::page(
        '',
        'Your share text comes here',
    )
    ->facebook()
    ->twitter()
    ->linkedin()
    ->telegram()
    ->whatsapp();
      return view('event', compact('event','shareComponent'));
     
      }

      public function event_details($id){
        $event = Event::find($id);
        Event::find($id)->increment('total_views');
        $views = Event::find($id);
        $sponsors = DB::table('events')
        ->join('image_gallery', 'events.id', '=', 'image_gallery.event')
        ->where('image_gallery.event', '=' ,$id)
        ->get();
        $host = DB::table('events')
        ->join('hosts', 'events.id', '=', 'hosts.event')
        ->where('hosts.event', '=' ,$id)
        ->get();
        $speakers = DB::table('events')
        ->join('speakers', 'events.id', '=', 'speakers.event')
        ->where('speakers.event', '=' ,$id)
        ->get();
        
      
        return view('/event_details', compact('event','views','sponsors','host','speakers'));
         
        }

        public function booking_msg(){
          return view('booking_msg');
        }

        public function pagenotfound()
{
    return view('errors.pagenotfound');
}

public function charts()
{

  $event = Event::all();
  $post = DB::table('blog_etc_posts')->select('id')->get();
  $booking = Booking::all();
$record = Booking::select(\DB::raw("COUNT(*) as count"), \DB::raw("DAYNAME(created_at) as day_name"), \DB::raw("DAY(created_at) as day"))
->where('created_at', '>', Carbon::today()->subDay(6))
->groupBy('day_name','day')
->orderBy('day')
->get();

 $data = [];

 foreach($record as $row) {
    $data['label'][] = $row->day_name;
    $data['data'][] = (int) $row->count;
  }

$data['chart_data'] = json_encode($data);
return view('/home', $data, compact('event', 'post', 'booking'));
}

       

}
