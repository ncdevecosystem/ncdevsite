<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Host extends Model
{
    use HasFactory;

    protected $table = 'hosts';


    protected $fillable = ['event','image','fullname','email','numbers','facebook','linkedin','twitter'];
}
