<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    protected $fillable = [
        'event', 'starting', 'ending', 'venue', 'description', 'audience', 'url' ,'image' ,'host' ,'cost' , 'categories' , 'slug'
    ];
}
